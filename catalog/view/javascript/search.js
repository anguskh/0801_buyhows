$(function(){
    
	var finish_tag = true;
    
    //$('#loading').removeClass('is-hidden');
    
    var $container = $('.albumWrap');
    $container.imagesLoaded(function(){
        $('#loading').addClass('is-hidden');
        $(".albums").fadeIn();
        $container.masonry({
            itemSelector: '.albums'
        });
    });
    
	$(window).scroll(function () 
	{
        if(finish_tag)
		{
            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 500)
			{
                finish_tag = false;
                $('#loading').removeClass('is-hidden');
                var offset      = $('#offset').val();
                var search      = $('#searchold').val();
                var activity    = $('#activity').val();
                $.ajax({
					url: 'index.php?route=common/search/ajaxActivitys&act=ajax&offset='+offset+'&search='+search+'&activity='+activity,
					success: function (data){
                        if (data.length>0)
						{
                            var $data = $(data).hide();
                            $container.append($data);
                            $container.imagesLoaded(function(){
                                $data.fadeIn();
                                $container.masonry( 'appended', $data, true );
                            });
                            $('#offset').val(parseInt(offset)+10);
							finish_tag = true;
						}
                        $('#loading').addClass('is-hidden');
					}
				});
			}
		}
	});
    
    // 搜尋列表
    $("#searchForm").submit(function(){
        var url = $(this).attr('action');
        var search = $("#search").val();
        if(search.length<=0){
            return false;
        }
        location = url+'&search='+encodeURIComponent(search);
        return false;
    });
    
})