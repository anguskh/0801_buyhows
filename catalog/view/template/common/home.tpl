<?php echo $header; ?>
    <div class="wrapper">
		<div class="header">
            <?php echo $header_bar; ?>
        </div>
		<div class="content">
			<div class="searchWrap">
				<div class="searchBox">
					<form action="<?php echo $search_action;?>" method="get" id="searchForm" class="searchForm clearfix" >
						<input type="search" name="search" id="search" placeholder="搜尋華麗身影" results="">
						<a class="searchKeyword" href="#" onclick="$('#searchForm').submit();">
							<div class="btnSearch"></div>
						</a>
					</form>
				</div>
			</div>
			<div class="albumWrap">
                <?php foreach($activitys as $k => $v){?>
                <a class="albums" href="<?php echo $v["activity_link"];?>">
					<div class="album_img" style="background-image:url(<?php echo $v["activity_cover"];?>);"></div>
                    <div class="album_info">
						<h2 class="album_title"><?php echo $v["activity_title"];?></h2>
					</div>
				</a>
                <?php }?>
            </div>
            <div id="loading">
				<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 75 75">
					<circle cx="37.5" cy="37.5" r="33.5"></circle>
				</svg>
			</div>
            <input type="hidden" id="offset" name="offset" value="<?php echo $offset;?>" />
		</div>
		<?php echo $footer; ?>
	</div>
    <script type="text/javascript" src="catalog/view/javascript/home.js"></script>
</body>
</html>