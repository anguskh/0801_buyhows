<!DOCTYPE html>
<html>
<head>
	<title>相片咖</title>
	<meta charset="utf-8">
    <meta name="description" content="<?php echo $description;?>" />
	<meta name="viewport" content="width=device-width">
    
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo $title;?>">
    <meta itemprop="description" content="<?php echo $description;?>">
    <meta itemprop="image" content="<?php echo $image;?>">
    
    <!-- Open Graph data -->
    <meta property="og:title" content="<?php echo $title;?>">
    <meta property="og:site_name" content="<?php echo $sitename;?>">
    <meta property="og:description" content="<?php echo $description;?>">
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?php echo $image;?>">
    <script>
            document.write('<meta property="og:url" content="'+location.href+'">');
    </script>
    
    <meta name="Author" content="BuyHows買好室 時價神器">
    <meta name="company" content="BuyHows買好室 時價神器">
    <meta name="robots" content="all" >
    <meta name="distribution" content="Taiwan">
    <meta name="copyright" content="© BuyHows買好室 時價神器 EventPal All rights reserved.">
    
    <link rel="icon" type="image/x-icon" href="catalog/view/image/favicon.ico">    
    <!-- CSS -->
	<link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/reset.min.css">
	<link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/photo.css?v=20180425170500">
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/iconeventpal.min.css">
    <!-- javascript Plugin -->
	<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/script.js"></script>
</head>
<body>