<pre>
    API 參考文件 後面有加的人也請在這裡補上
    1. 取得台灣地區縣市名稱
        http://bs.anguskh.com/?route=api/buyhows/getCity
    2. 取得台灣地區縣市之次行政區
        http://bs.anguskh.com/?route=api/buyhows/getCityArea&twCity=宜蘭縣
    3. 取得建案種類
        http://bs.anguskh.com/?route=api/buyhows/getBuildingType
    4. 取得交易類型
        http://bs.anguskh.com/?route=api/buyhows/getBuyType
    5. 查詢地址
        http://bs.anguskh.com/?route=api/buyhows/search&addr=新北市中和區中山路
    6. 查詢地圖
        http://bs.anguskh.com/?route=api/buyhows/searchmap&addr=新北市中和區中山路
</pre>
