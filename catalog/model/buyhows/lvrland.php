<?php
class ModelBuyhowsLvrland extends Model {

	/**
	 * [getTwCity TW縣市名稱]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-09-01
	 */
	public function getTwCity( $data = [])
	{
		$SQLCmd = "SELECT DISTINCT tw_city FROM map_main" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [getTwCityArea TW縣市的各區名稱]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-09-01
	 */
	public function getTwCityArea( $data = [])
	{
		$SQLCmd = "SELECT DISTINCT tw_city_area FROM map_main WHERE tw_city='".$this->db->escape($data['tw_city'])."'" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [getBuildingType 現行建物類型]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-09-01
	 */
	public function getBuildingType( $data = [])
	{
		$SQLCmd = "SELECT DISTINCT building_type FROM map_main ORDER BY building_type ASC " ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [getBuyType 交易類型]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-09-01
	 */
	public function getBuyType( $data = [])
	{
		$SQLCmd = "SELECT DISTINCT buy_type FROM map_main ORDER BY buy_type ASC " ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [search 查詢地址範例]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-09-01
	 */
	public function search( $data = [])
	{
		$SQLCmd = "SELECT DISTINCT address FROM map_main
			WHERE address LIKE '%".$this->db->escape($data['address'])."%' AND buy_type NOT IN('車位','土地')
				AND ( lat IS NOT NULL AND lng IS NOT NULL)
				ORDER BY address ASC" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [searchmap description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another min
	 * @date    2020-09-01
	 */
	public function searchmap( $data = [])
	{
		$where = "";
		$now_longitude = "";
		$now_latitude = "";
		if(isset($data['address']))
		{
			$where .= "address LIKE '".$this->db->escape($data['address'])."%'";
			$SQLCmd = "SELECT tw_city,tw_city_area,lat,lng,buy_type,address,jiaoyi_items,building_type,completed_date,total_price 
						FROM map_main 
						WHERE {$where} AND buy_type !='車位' AND buy_type !='土地' 
						AND (lat IS NOT NULL AND lng IS NOT NULL AND lat !=0 AND lng!=0)
						ORDER BY address ASC" ;
			$query = $this->db->query( $SQLCmd) ;
			$now_longitude = $query->rows[0]['lng'];
			$now_latitude = $query->rows[0]['lat'];
		}

		if(isset($data['lat']) && isset($data['lng']))
		{
			$now_longitude = $data['lng'];
			$now_latitude = $data['lat'];
			$SQLCmd = "SELECT tw_city,tw_city_area,lat,lng,buy_type,address,jiaoyi_items,building_type,completed_date,total_price,( 6371 * acos( cos( radians({$now_latitude}) ) * cos( radians( mm.lat ) ) * cos( radians( mm.lng ) - radians({$now_longitude}) ) + sin( radians({$now_latitude}) ) * sin( radians( mm.lat ) ) ) ) AS distance 
					FROM map_main mm 
					WHERE mm.buy_type !='車位' AND mm.buy_type !='土地' 
					HAVING distance <= 2
					ORDER BY distance ";
		}
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [getnearbyaddress description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another min
	 * @date    2020-09-01
	 */
	public function getdetail( $data = [])
	{
		$where = "";
		$now_longitude = "";
		$now_latitude = "";
		if(isset($data['address']))
		{
			$where .= " AND address LIKE '".$this->db->escape($data['address'])."%'";
			$SQLCmd = "SELECT tw_city,tw_city_area,lat,lng,buy_type,address,jiaoyi_items,building_type,completed_date,total_price 
						FROM map_main 
						WHERE buy_type !='車位' AND buy_type !='土地' 
						{$where} 
						AND (lat IS NOT NULL AND lng IS NOT NULL AND lat !=0 AND lng!=0) 
						ORDER BY address ASC" ;
			$query = $this->db->query( $SQLCmd) ;
			$now_longitude = $query->rows[0]['lng'];
			$now_latitude = $query->rows[0]['lat'];
		}

		if(isset($data['lat']) && isset($data['lng']))
		{
			if($data['lat']!=""&&$data['lng']!="")
			{
				$now_longitude = $data['lng'];
				$now_latitude = $data['lat'];
			}
		}

		if(isset($data['building_type_sn']))
		{
			$where .= " AND building_type_sn = '{$data['building_type_sn']}'";
		}

		if(isset($data['minyear']) && isset($data['maxyear']))
		{
			$nowyear= date("Y");
			$where .= " AND ({$nowyear} - completed_year) BETWEEN {$data['minyear']} AND {$data['maxyear']}"; 
		}

		if($now_longitude!="" && $now_latitude!="")
		{
			$SQLCmd = "SELECT tw_city,tw_city_area,lat,lng,buy_type,address,jiaoyi_items,building_type,completed_date,total_price,( 6371 * acos( cos( radians({$now_latitude}) ) * cos( radians( mm.lat ) ) * cos( radians( mm.lng ) - radians({$now_longitude}) ) + sin( radians({$now_latitude}) ) * sin( radians( mm.lat ) ) ) ) AS distance 
					FROM map_main mm 
					WHERE mm.buy_type !='車位' AND mm.buy_type !='土地' 
					{$where} 
					AND (lat IS NOT NULL AND lng IS NOT NULL AND lat !=0 AND lng!=0) 
					HAVING distance <= 0.2
					ORDER BY distance ";
			$query = $this->db->query( $SQLCmd) ;
		}
		else
		{
			return array();
		}
		// echo $SQLCmd;
		return $query->rows ;
	}

	/**
	 * @param array $data
	 */
	public function getnearbyaddress( $data = [])
	{
		if(isset($data['lat']) && isset($data['lng']))
		{
			$now_longitude = $data['lng'];
			$now_latitude = $data['lat'];
			$SQLCmd = "SELECT address,( 6371 * acos( cos( radians({$now_latitude}) ) * cos( radians( mm.lat ) ) * cos( radians( mm.lng ) - radians({$now_longitude}) ) + sin( radians({$now_latitude}) ) * sin( radians( mm.lat ) ) ) ) AS distance
					FROM map_main mm
					WHERE mm.buy_type NOT IN('車位','土地')
					HAVING distance < 1
					ORDER BY distance
					limit 1";
			$query = $this->db->query( $SQLCmd) ;
			return $query->rows ;
		}
		else
			return "";
	}

	/**
	 * [getprice description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-09-01
	 */
	public function getprice($data = [])
	{
		$nowyear= date("Y");
		if(isset($data['minyear']) && isset($data['maxyear']))
		{
			$where = "";
			if(isset($data['building_type_sn']))
			{
				$where .= " AND building_type_sn = '{$data['building_type_sn']}'";
			}
			if(isset($data['address']))
			{
				$where .= " AND address LIKE '{$data['address']}%'";
			}
			$minyear = $data['minyear'];
			$maxyear = $data['maxyear'];
			$SQLCmd = "SELECT MAX(round(price_square/10000 * 3.3)) as max_price,MIN(round(price_square/10000 * 3.3)) as min_price,building_type_sn
						FROM map_main
						WHERE buy_type NOT IN('車位','土地')
						AND ({$nowyear} - completed_year) BETWEEN {$minyear} AND {$maxyear}
						{$where}
						ORDER BY price_square";
		}
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [getpricerange description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-09-01
	 */
	public function getpricerange($data = [])
	{
		$nowyear= date("Y");
		if(isset($data['minyear']) && isset($data['maxyear']))
		{
			$where = "";
			if(isset($data['building_type_sn']))
			{
				$where .= " AND building_type_sn = '{$data['building_type_sn']}'";
			}
			if(isset($data['address']))
			{
				$where .= " AND address LIKE '{$data['address']}%'";
			}
			$minyear = $data['minyear'];
			$maxyear = $data['maxyear'];
			$SQLCmd = "SELECT FLOOR(price_square/10000 * 3.3/10) price_square, count(*) count_num
						FROM map_main
						WHERE buy_type NOT IN ('車位', '土地') AND (lat IS NOT NULL AND lng IS NOT NULL)
						{$where} AND ({$nowyear} - completed_year) BETWEEN {$minyear} AND {$maxyear}
						GROUP BY FLOOR(price_square/10000 * 3.3/10)
						ORDER BY price_square ASC";
		}
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}
}