<?php
class ControllerCommonSearch extends Controller {
    
    /**
	 * [search 相簿搜尋]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-03-29
	 */
	public function index() {
        $server = $this->config->get('serverLink');
        
        // get 取回的資料========================================================================
        $search     = isset($this->request->get['search']) ? trim($this->request->get['search']) : "";
        $activity   = isset($this->request->get['activity']) ? trim($this->request->get['activity']) : "";
        if(empty($search) && empty($activity)){
            $this->response->redirect($this->url->link('common/home',''));
			exit();
        }
        
        // 準備列表資料==========================================================================
        $this->load->model('catalog/album');
        $this->load->model('catalog/activity');
        
        $data["albums"]     = array();
        $data["activitys"]  = array();
        $filterArr = array(
            "status"    => 2,
            "search"    => $search,
            "activity"  => $activity,
            "start"     => 0,
            "limit"     => 10
        );
        $albumsArr = $this->model_catalog_album->getLists($filterArr);
        if(count($albumsArr)){
            $this->load->model('tool/image');
            foreach($albumsArr as $album){
                $cover_image = '';
                if(!empty($album["cover_image"])){
                    $cover_image = $this->model_tool_image->resizePhoto($album["cover_image"]);
                }
                elseif(!empty($album["first_image"])){
                    $cover_image = $this->model_tool_image->resizePhoto($album["first_image"]);
                }
                if(!$cover_image){
                    $cover_image = $server.'image/catalog/album_default.jpg';
                }
                $data["albums"][] = array(
                    "album_name"  => $album["name"],
                    "album_cover" => $cover_image,
                    "album_link"  => $this->url->link('photo/photo', 'album=' . $album["album_id"], true)
                );
            }
            // 設定 meta data===============================================================
            $this->document->setTitle($data["albums"][0]["album_name"]);
            $this->document->setDescription($data["albums"][0]["album_name"]);
            $this->document->setImages($data["albums"][0]["album_cover"]);
        }
        
        // 廣告資料(推薦賽事)========================================================================
        $nowDate = date("Y-m-d H:i:s");
        $filterArr = array(
            "sdate" => $nowDate,
            "edate" => $nowDate,
            "flag"  => 1,
            "start" => 0,
            "limit" => count($data["albums"]) ? count($data["albums"]) : 10
        );
        $activityArr = $this->model_catalog_activity->getDefaltList($filterArr);
        if(count($activityArr)){
            foreach($activityArr as $k => $v){
                $data["activitys"][] = array(
                    "img"   => $v["imgurl"],
                    "link"  => $v["pagelink"]
                );
            }
        }
        
        $data["search"]         = $search;
        $data["activity"]       = $activity;
        $data["offset"]         = 10;
        $data["search_action"]  = $this->url->link('common/search', '', true);
        
        // 程式最後 ==============================================================================
        $data['header']     = $this->load->controller('common/header');
        $data['header_bar'] = $this->load->controller('common/header/navBar');
        $data['footer']     = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/search', $data));
	}
    
    /**
	 * [ajaxActivitys 瀑布流資料]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-04-23
	 */
    public function ajaxActivitys(){
        $server = $this->config->get('serverLink');
        
        // get 取回的資料=========================================================================
        $act        = isset($this->request->get['act']) ? $this->request->get['act'] : "";
        $offset     = isset($this->request->get['offset']) ? intval($this->request->get['offset']) : 10;
        $search     = isset($this->request->get['search']) ? trim($this->request->get['search']) : "";
        $activity   = isset($this->request->get['activity']) ? trim($this->request->get['activity']) : "";
        
        // 準備資料==============================================================================
        $responseHtml = '';
        if($act=='ajax'){
            $this->load->model('catalog/activity');
            $this->load->model('catalog/album');
            $filterArr = array(
                "start"     => $offset,
                "limit"     => 20
            );
            $albumsArr = $this->model_catalog_album->getLists($filterArr);
            if(count($albumsArr)){
                $this->load->model('tool/image');
                foreach($albumsArr as $album){
                    $cover_image = '';
                    if(!empty($album["cover_image"])){
                        $cover_image = $this->model_tool_image->resizePhoto($album["cover_image"]);
                    }
                    elseif(!empty($album["first_image"])){
                        $cover_image = $this->model_tool_image->resizePhoto($album["first_image"]);
                    }
                    if(!$cover_image){
                        $cover_image = $server.'image/catalog/album_default.jpg';
                    }
                    $album_link = $this->url->link('photo/photo', 'album=' . $album["album_id"], true);
                    $responseHtml .= '<a class="albums" href="'.$album_link .'">';
                    $responseHtml .= '<div class="album_img" style="background-image:url('.$cover_image.')"></div>';
                    $responseHtml .= '<div class="album_info">';
                    $responseHtml .= '<h2 class="album_title">'.$album["name"].'</h2>';
                    $responseHtml .= '</div>';
                    $responseHtml .= '</a>';
                }
            }
        }
        
        $this->response->setOutput($responseHtml);
    }
}
