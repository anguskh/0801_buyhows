<?php
class ControllerPhotoPhoto extends Controller {
	public function index() {
        $server = $this->config->get('serverLink');
        // get 取回的資料========================================================================
        $album  = isset($this->request->get['album']) ? trim($this->request->get['album']) : "";
        $photo  = isset($this->request->get['photo']) ? trim($this->request->get['photo']) : "";
        $bib    = isset($this->request->get['bib']) ? trim($this->request->get['bib']) : "";
        $stime  = isset($this->request->get['stime']) ? $this->request->get['stime'] : "";
        $etime  = isset($this->request->get['etime']) ? $this->request->get['etime'] : "";
        $page   = isset($this->request->get['page']) ? intval($this->request->get['page']) : 1;
        
        // 取得相簿資料==========================================================================
        if(empty($album)){
            $this->response->redirect($this->url->link('common/home',''));
			exit();
        }
        $this->load->model('catalog/album');
        $albumArr = $this->model_catalog_album->getAlbum($album);
        if(!count($albumArr)){
            $this->response->redirect($this->url->link('common/home',''));
            exit();
        }
        $data["album"] = array(
            "id"        => $albumArr["album_id"],
            "name"      => $albumArr["name"],
            "date"      => $albumArr["shoot_start"],
            "maxTime"   => "00:00",
            "minTime"   => "00:00",
            "link"      => $this->url->link('photo/photo','&album='.$album)
        );
        // 相簿開始與結束時間========================================================================
        $this->load->model('catalog/product');
        $this->load->model('catalog/activity');
        $this->load->model('tool/image');
        $times = $this->model_catalog_product->getProductTimes($albumArr["album_id"]);
        if(count($times)){
            $data["album"]["maxTime"] = $times["maxTime"];
            $data["album"]["minTime"] = $times["minTime"];
        }
        // 拍攝區間調整(10分鐘一區間)
        $maxTime = (int)substr($data["album"]["maxTime"],0,2)*60+ceil((int)substr($data["album"]["maxTime"],3,2)/10)*10;
        $minTime = (int)substr($data["album"]["minTime"],0,2)*60+floor((int)substr($data["album"]["minTime"],3,2)/10)*10;
        $data["album"]["maxTime"] = str_pad(floor($maxTime/60),2,"0",STR_PAD_LEFT).":".str_pad($maxTime%60,2,"0",STR_PAD_LEFT);
        $data["album"]["minTime"] = str_pad(floor($minTime/60),2,"0",STR_PAD_LEFT).":".str_pad($minTime%60,2,"0",STR_PAD_LEFT);
        
        $stime = !empty($stime) ? $stime : $data["album"]["minTime"];
        $etime = !empty($etime) ? $etime : $data["album"]["maxTime"];
        
        // 準備列表資料==========================================================================
        $data["photos"]     = array();
        $data["activitys"]  = array();
        $filterArr = array(
            "album_id"  => $album,
            "product"   => $photo,
            "bib"       => $bib,
            "stime"     => $albumArr["shoot_start"]." ".$stime.":00",
            "etime"     => $albumArr["shoot_end"]." ".$etime.":00",
            "status"    => 1,
            "start"     => ($page-1)*18,
            "limit"     => 18
        );
        $productsCnt = $this->model_catalog_product->getTotalCnt($filterArr);
        if($productsCnt){
            $productsArr = $this->model_catalog_product->getLists($filterArr);
            if(count($productsArr)){
                foreach($productsArr as $i => $product){
                    $photo_image = '';
                    $cover_image = '';
                    if(!empty($product["image"])){
                        $photo_image = $this->model_tool_image->resizePhoto($product["image"]);
                        $cover_image = $this->model_tool_image->waterPhoto($product["image"]);
                    }
                    $data["photos"][] = array(
                        "photo_id"      => $product["product_id"],              //照片編號
                        "photo_bib"     => $product["bib"],                     //號碼布
                        "photo_time"    => substr($product["date_photo"],11,5), //拍攝時間
                        "photo_img"     => !empty($photo_image) ? $photo_image : $server.'image/catalog/album_default.jpg', //縮圖
                        "photo_serial"  => ($page-1)*18+($i+1),                 //流水號
                        "photo_date"    => substr($product["date_photo"],0,16), //拍攝日期
                        "photo_cover"   => !empty($cover_image) ? $cover_image : $server.'image/catalog/album_default.jpg', //壓圖
                        "photo_param"   => "album={$album}&photo={$photo}&bib={$bib}&stime={$stime}&etime={$etime}&page={$page}&product=".$product["product_id"],
                        "photo_share"   => $this->url->link('photo/photo', '&album='.$product["album_id"].'&photo='.$product["product_id"], true)
                    );
                }
            }
        }
        else{
            // 廣告資料(推薦賽事)========================================================================
            $nowDate = date("Y-m-d H:i:s");
            $filterArr = array(
                "sdate" => $nowDate,
                "edate" => $nowDate,
                "flag"  => 1,
                "start" => 0,
                "limit" => 10
            );
            $activityArr = $this->model_catalog_activity->getDefaltList($filterArr);
            if(count($activityArr)){
                foreach($activityArr as $k => $v){
                    $data["activitys"][] = array(
                        "img"   => $v["imgurl"],
                        "link"  => $v["pagelink"]
                    );
                }
            }
        }
        
        $data["bib"]            = $bib;
        $data["photo"]          = $photo;
        $data["stime"]          = $stime;
        $data["etime"]          = $etime;
        $data["search_action"]  = $this->url->link('photo/photo', '', true);
        $data["totalCnt"]       = $productsCnt;
        
        // 分頁處理==================================================================================
		$pagination = new Pagination();
		$pagination->total = $productsCnt;
		$pagination->page = $page;
		$pagination->limit = 18;
		$pagination->url = $this->url->link("photo/photo","&album={$album}&bib={$bib}&stime={$stime}&etime={$etime}");
		
		$data['pagination'] = $pagination->make();
        
        // 設定 meta data==========================================================================
        $coverImage = $server.'image/catalog/album_default.jpg';
        if(!empty($albumArr["cover_image"])){
            $coverImage = $this->model_tool_image->resizePhoto($albumArr["cover_image"]);
        }
        if(!empty($photo) && !empty($data["photos"][0]["photo_img"])){
            $coverImage = $data["photos"][0]["photo_img"];
        }
        $this->document->setTitle($albumArr["name"]);
		$this->document->setDescription($albumArr["name"]);
        $this->document->setImages($coverImage);
        
        // 程式最後 ==============================================================================
        $data['header']     = $this->load->controller('common/header');
        $data['header_bar'] = $this->load->controller('common/header/navBar');
        $data['footer']     = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('photo/photo', $data));
	}
    
    /**
	 * [ajaxClick 新增相片點擊]
	 * @Another Nicole
	 * @date    2018-04-25
	 */
    public function ajaxClick(){
        // get 取回的資料=========================================================================
        $act = isset($this->request->get['act']) ? intval($this->request->get['act']) : "";
        $product = isset($this->request->get['product']) ? intval($this->request->get['product']) : "";
        
        // 準備資料==============================================================================
        $response = true;
        if(!empty($product) && $act=="ajax"){
            $this->load->model('catalog/product');
            $insertArr = array(
                "item_id" => $product,
                "type"    => 2
            );
            $this->model_catalog_product->insertClickRecord( $insertArr);
        }
        
        $this->response->setOutput($response);
    }
    
    public function download(){
        // get 取回的資料=========================================================================
        $product = isset($this->request->get['product']) ? intval($this->request->get['product']) : "";
        
        $url = '';
        $paramArr = array("album","photo","bib","stime","etime","page");
        foreach($paramArr as $param){
            if(isset($this->request->get["{$param}"]) && !empty($this->request->get["{$param}"])){
                $url .= "&{$param}=".$this->request->get["{$param}"];
            }
        }
        if(empty($product)){
            $this->response->redirect($this->url->link('photo/photo',$url));
            exit();
        }
        $this->load->model('catalog/product');
        $product = $this->model_catalog_product->getProduct( $product);
        if(!count($product) || empty($product["image"]) || !file_exists(DIR_IMAGE. $product["image"])){
            $this->response->redirect($this->url->link('photo/photo',$url));
            exit();
        }
        
        $size = @getimagesize(DIR_IMAGE. $product["image"]);
        $fp = @fopen(DIR_IMAGE. $product["image"], "rb");
        if ($size && $fp)
        {
            $extension  = pathinfo($product["image"], PATHINFO_EXTENSION);
            $tempName   = pathinfo($product["image"], PATHINFO_FILENAME);
            $filename   = md5($tempName).".".$extension;
            
            header("Content-type: {$size['mime']}");
            header("Content-Length: " . filesize(DIR_IMAGE. $product["image"]));
            header("Content-Disposition: attachment; filename=$filename");
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
            fpassthru($fp);
            exit;
        }
        $this->response->redirect($this->url->link('photo/photo',$url));
    }
    
    public function collection(){
        // get 取回的資料=========================================================================
        $product = isset($this->request->get['product']) ? intval($this->request->get['product']) : "";
        
        $url = '';
        $paramArr = array("album","photo","bib","stime","etime","page");
        foreach($paramArr as $param){
            if(isset($this->request->get["{$param}"]) && !empty($this->request->get["{$param}"])){
                $url .= "&{$param}=".$this->request->get["{$param}"];
            }
        }
        
        
        $this->response->redirect($this->url->link('photo/photo',$url));
    }
    
}
