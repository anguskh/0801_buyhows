<?php
class ControllerApiBuyhows extends Controller {
	private $start_time = 0 ;
	private $json       = [] ;
	private $error      = [] ;

    public function __construct($registry)
    {
	    parent::__construct($registry);
	    $this->load->model('buyhows/lvrland');
	    $this->start_time = microtime(true) ;
    }

	/**
	 * 起手式
	 */
    public function index()
    {
	    $data['header'] = $this->load->controller('common/header');
//	    $data['column_left'] = $this->load->controller('common/column_left');
//	    $data['header'] = "";
	    $data['column_left'] = "";
	    $data['footer'] = $this->load->controller('common/footer');

	    $this->response->setOutput($this->load->view('api/document', $data));
    }

	/**
	 * [getCity 取得台灣地區縣市名稱]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-08-28
	 */
    public function getCity()
    {
//	    $filter_data = array(
//		    'filter_name' => $this->request->get['filter_name'],
//		    'start'       => 0,
//		    'limit'       => 5
//	    );

	    $filter_data = [] ;
	    $results = $this->model_buyhows_lvrland->getTwCity($filter_data);
	    $this->json['count'] = count( $results) ;
	    foreach ( $results as $iCnt => $res) {
	    	$tmp[] = $res ;
	    }

	    $this->setOutput( $tmp) ;
    }

	/**
	 * [getCityArea 取得台灣地區縣市之次行政區]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-08-28
	 */
	public function getCityArea()
	{
		$json = [] ;
		$twCity = $this->request->get['twCity'] ;
		$filter_data = [ 'tw_city' => $twCity] ;

		$results = $this->model_buyhows_lvrland->getTwCityArea($filter_data);
		$this->json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}
		$this->json['filter'] = $filter_data ;
		$this->setOutput( $tmp) ;
	}

	/**
	 * [getBuildingType 取得建案種類]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-08-28
	 */
	public function getBuildingType()
	{
		$json = [] ;
		$filter_data = [] ;

		$results = $this->model_buyhows_lvrland->getBuildingType($filter_data);
		$this->json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}

		$this->setOutput( $tmp) ;
	}

	/**
	 * [getBuyType 取得交易類型]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-08-28
	 */
	public function getBuyType()
	{
		$json = [] ;
		$filter_data = [] ;

		$results = $this->model_buyhows_lvrland->getBuyType($filter_data);
		$this->json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}

		$this->setOutput( $tmp) ;
	}

	/**
	 * [search description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-08-28
	 */
	public function search()
	{
		$json = [] ;
		$address = $this->request->get['addr'] ;
		$filter_data = [ 'address' => $address] ;

		$results = $this->model_buyhows_lvrland->search($filter_data);
		$json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}

		$this->setOutput( $tmp) ;
	}

	/**
	 * [search description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-08-28
	 */
	public function searchmap()
	{
		$json = [] ;
		if(isset($this->request->get['addr']))
		{
			$address = $this->request->get['addr'] ;
			$filter_data['address'] = $address ;
		}

		if(isset($this->request->get['lat']) && isset($this->request->get['lng']))
		{
			$filter_data['lat'] = $this->request->get['lat'] ;
			$filter_data['lng'] =$this->request->get['lng'] ;
		}

		$results = $this->model_buyhows_lvrland->searchmap($filter_data);
		$json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}
		$json['run_time'] = microtime(true) - $this->start_time ;
		$json['data']     = $tmp ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [searchdetail description]
	 * @return  [type]     [description]
	 * @Another ming
	 * @date    2020-09-01
	 */
	public function getdetail()
	{
		$json = [] ;
		if(isset($this->request->get['addr']))
		{
			$address = $this->request->get['addr'] ;
			$filter_data['address'] = $address ;
		}
		
		if(isset($this->request->get['building_type_sn']))
		{
			$filter_data['building_type_sn'] = $this->request->get['building_type_sn'] ;
		}

		if(isset($this->request->get['lat']) && isset($this->request->get['lng']))
		{
			$filter_data['lat'] = $this->request->get['lat'] ;
			$filter_data['lng'] =$this->request->get['lng'] ;
		}

		if(isset($this->request->get['minyear']) && isset($this->request->get['maxyear']))
		{
			$filter_data['minyear'] = $this->request->get['minyear'] ;
			$filter_data['maxyear'] = $this->request->get['maxyear'] ;
		}

		$results = $this->model_buyhows_lvrland->getdetail($filter_data);
		$json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}
		if(!isset($tmp))
		{
			$tmp = null;
		}
		$json['run_time'] = microtime(true) - $this->start_time ;
		$json['data']     = $tmp ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [setOutput 輸出json format]
	 * @param   [type]     $data [description]
	 * @Another Angus
	 * @date    2020-08-29
	 */
	public function setOutput( $data) {
		$this->json['run_time'] = microtime(true) - $this->start_time ;
		$this->json['data']     = $data ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($this->json));
	}

	/**
	 * 範例
	 */
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/attribute');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_attribute->getAttributes($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'attribute_id'    => $result['attribute_id'],
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'attribute_group' => $result['attribute_group']
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));

	}

	public function getprice()
	{
		$json = [] ;

		if(isset($this->request->get['minyear']) && isset($this->request->get['maxyear']))
		{
			$filter_data['minyear'] = $this->request->get['minyear'] ;
			$filter_data['maxyear'] =$this->request->get['maxyear'] ;
		}

		if(isset($this->request->get['building_type_sn']))
		{
			$filter_data['building_type_sn'] = $this->request->get['building_type_sn'] ;
		}

		if(isset($this->request->get['addr']))
		{
			if($this->request->get['addr']!="")
				$filter_data['address'] = $this->request->get['addr'] ;
		}

		$results = $this->model_buyhows_lvrland->getprice($filter_data);
		$json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}

		$json['run_time'] = microtime(true) - $this->start_time ;
		$json['data']     = $tmp ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getnearbyaddress()
	{
		$json = [] ;

		if(isset($this->request->get['lat']) && isset($this->request->get['lng']))
		{
			$filter_data['lat'] = $this->request->get['lat'] ;
			$filter_data['lng'] =$this->request->get['lng'] ;
		}

		$results = $this->model_buyhows_lvrland->getnearbyaddress($filter_data);
		$json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}

		$json['run_time'] = microtime(true) - $this->start_time ;
		$json['data']     = $tmp ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getpricerange()
	{
		$json = [] ;

		if(isset($this->request->get['minyear']) && isset($this->request->get['maxyear']))
		{
			$filter_data['minyear'] = $this->request->get['minyear'] ;
			$filter_data['maxyear'] = $this->request->get['maxyear'] ;
		}

		if(isset($this->request->get['building_type_sn']))
		{
			$filter_data['building_type_sn'] = $this->request->get['building_type_sn'] ;
		}
		if(isset($this->request->get['address']))
		{
			$filter_data['address'] = $this->request->get['address'] ;
		}

		$results = $this->model_buyhows_lvrland->getpricerange($filter_data);
		$json['count'] = count( $results) ;
		foreach ( $results as $iCnt => $res) {
			$tmp[] = $res ;
		}

		$json['run_time'] = microtime(true) - $this->start_time ;
		$json['data']     = $tmp ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}