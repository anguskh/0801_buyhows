<?php session_start();
    // $mapkey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ2aWV3aWVzMTAxOUBnbWFpbC5jb20iLCJuYW1lIjoidmlld2llczEwMTlAZ21haWwuY29tIiwiaWF0IjoxNTk3OTA4MzQ2LCJvYmplY3RzIjpbIlwvbWFwc1wvanMiLCJcL2RhdGEiLCJcL3N0eWxlcyIsIlwvc3ByaXRlcyIsIlwvZm9udHMiXSwiZXhwIjoxNjAxNzA5OTQ2fQ.PifGcAMtO4PdIsEEWMUJMXHaR3miKASZoHAQ6Rtn3gc";
    $apiurl = "https://bs.anguskh.com/";
    $mapkey = "";
    // $apiurl = "http://127.0.0.1/buyhows/";
    if(!isset($_SESSION['building_type']))
        $_SESSION['building_type'] = "";
    if(!isset($_SESSION['searchkey']))
        $_SESSION['searchkey'] = "";
    if(!isset($_SESSION['lat']))
        $_SESSION['lat'] = "";
    if(!isset($_SESSION['lng']))
        $_SESSION['lng'] = "";
    if(isset($_POST['yeartype']))
        $_SESSION['yeartype'] = ($_POST['yeartype']!==null)?$_POST['yeartype']:$_SESSION['yeartype'];

    $building_type = "";
    if(isset($_SESSION['building_type']))
    {
        switch($_SESSION['building_type'])
        {
            case 1:
                $building_type = "公寓";
                break;
            case 2:
                $building_type = "大樓";
                break;
            case 3:
                $building_type = "透天";
                break;
        }
    }
    
    for($i=1;$i<=5;$i++)
    {
        ${'select'.$i} = "";
    }
    if(isset($_SESSION['yeartype']))
        ${'select'.$_SESSION['yeartype']} = 'selected = "selected"';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>買好室｜實價神器</title>
    <!-- <link rel="stylesheet" href="https://api.map8.zone/css/gomp.css?key=<?php echo $mapkey;?>" /> -->
    <link href="css/fontawesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/easy-autocomplete.min.css" rel="stylesheet">
    <script type='text/javascript' src='js/jquery-1.11.2.min.js'></script>
    <script type='text/javascript' src='js/jquery.easy-autocomplete.min.js'></script> 
    <link rel="stylesheet" href="css/leaflet/leaflet.css" />
    <link href="css/leaflet/MarkerCluster.css"></link> 
    <!-- <link href="css/leaflet//MarkerCluster.Default.css"></link>  -->
    <script src="js/leaflet/leaflet.js"></script>
    <script src="js/leaflet/leaflet.markercluster.js"></script>
    <style>
        a{
            cursor: pointer;
        }

        .cluster {
            border: 2px solid grey;
            border-radius: 150px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .cluster.cluster-green {
            background-color: green;
        }

        .cluster.cluster-yellow {
            background-color: yellow;
        }

        .cluster.cluster-red {
            background-color: red;
        }
    </style>
</head>
<body>
    <header>
        <img src="http://buyhows.com/wp-content/uploads/2020/05/%E6%9A%AB%E6%99%82LOGOR4.png" alt=""><span>實價神器</span>
    </header>
    <div class="result result-on-map" id="topbar" style="display:none">
        <div class="statement">
            <select name="" id="">
                <option value="">即時定位</option>
            </select>
            <span><?php echo $building_type;?></span>
            <select name="houseyear" id="houseyear">
                <option value="1" <?php echo $select1;?>>0 ~ 10</option>
                <option value="2" <?php echo $select2;?>>11 ~ 20</option>
                <option value="3" <?php echo $select3;?>>21 ~ 30</option>
                <option value="4" <?php echo $select4;?>>31 ~ 40</option>
                <option value="5" <?php echo $select5;?>>41 ~ 50</option>
            </select>
        </div>
    </div>
    <div id="searchform" class="search" style="display:none">
        <form action="">
            <div class="search_zone">
                <p>關鍵字搜尋</p>
                <p><input id="basics" type="text" placeholder="建案名稱或地址"><a href="#" id="search_btn" class="search_btn"><i class="fas fa-search"></i></a></p>
            </div>
            <div class="search_zone">
                <p>依建案年份搜尋</p>
                <p>
                    <select name="houseyear" id="houseyear">
                        <option value="1" <?php echo $select1;?>>0 ~ 10年</option>
                        <option value="2" <?php echo $select2;?>>11 ~ 20年</option>
                        <option value="3" <?php echo $select3;?>>21 ~ 30年</option>
                        <option value="4" <?php echo $select4;?>>31 ~ 40年</option>
                        <option value="5" <?php echo $select5;?>>41 ~ 50年</option>
                    </select>
                    <a class="search_btn" id="searchprice"><i class="fas fa-search"></i></a>
                    <a id="getlocation" class="search_btn search_btn--location"><i class="fas fa-location-arrow"></i></a>
                </p>
                <a id="apartment_result" class="search_category">
                    <span class="search_category--type">公寓</span>
                    <span class="search_category--price" id="apartment_price_range">0 ~ 0萬</span>
                    <i class="fas fa-arrow-right"></i>
                </a>
                <a id="building_result" class="search_category">
                    <span class="search_category--type">大樓</span>
                    <span class="search_category--price" id="building_price_range">0 ~ 0萬</span>
                    <i class="fas fa-arrow-right"></i>
                </a>
                <a id="toutiancuo_result" class="search_category">
                    <span class="search_category--type">透天</span>
                    <span class="search_category--price" id="toutiancuo_price_range">0 ~ 0萬</span>
                    <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </form>
    </div>
    <div id="loadingIMG" class="loading" style="display: block;">
        <div id="img_label" class="img_label"><span lkey="inquire_data_wait">資料搜尋中，請稍候。</span></div>
    </div>
    <!-- <div class="statement" id="statement">
        <select name="" id="">
            <option value="">即時定位</option>
        </select>
        <span><?php echo $building_type;?></span>
        <select name="houseyear" id="houseyear">
            <option value="1" <?php echo $select1;?>>0 ~ 10</option>
            <option value="2" <?php echo $select2;?>>11 ~ 20</option>
            <option value="3" <?php echo $select3;?>>21 ~ 30</option>
            <option value="4" <?php echo $select4;?>>31 ~ 40</option>
            <option value="5" <?php echo $select5;?>>41 ~ 50</option>
        </select>
    </div> -->
    <!-- <div id="div1"><button id="bt1">文字按鈕</button><button id="bt2">文字按鈕</button><button id="bt3">文字按鈕</button><button id="bt4">文字按鈕</button></div> -->
    <div class="mapbox">
        <div id="div2"><div id="leftbar" style="display:none"><button id="sheet">sheet</button><button id="complex">綜合</button><button id="streetView">街景</button><button id="navigation">導行</button><button id="transYear">交易年</button></div></div>
        <div id="div3"><button id="bt2_1"><i class="fas fa-plus"></i></button><button id="bt2_2"><i class="fas fa-minus"></i></button><button id="bt2_3"><i class="fas fa-arrows-alt"></i></button><button id="bt2_4"><i class="fas fa-search"></i></button></div>
        <div id="postresult" style="display:none">
            <form id="postform" action="search_result_new.php" method="post">
                <input id="building_type" name="building_type" value=<?php echo $_SESSION['building_type'];?>>
                <input id="yeartype" name="yeartype" value=<?php echo $_SESSION['yeartype'];?>>
                <input id="searchkey" name="searchkey" value=<?php echo $_SESSION['searchkey'];?>>
                <input id="lng" name="lng" value=<?php echo $_SESSION['lng'];?>>
                <input id="lat" name="lat" value=<?php echo $_SESSION['lat'];?>>
            </form>
        </div>
        <div id="map" class="map_fixed"></div>
    </div>
    <div class="result result-bottom" id="bottombar" style="display:none">
        <div class="table-responsive" id="detaildiv" >
            <table class="table" id="detaildata">
                <thead>
                    <tr>
                        <th scope="col">序號</th>
                        <th scope="col">縣市</th>
                        <th scope="col">區</th>
                        <th scope="col">交易標的</th>
                        <th scope="col">格局</th>
                        <th scope="col">類型</th>
                        <th scope="col">完工日期</th>
                        <th scope="col">總價</th>
                    </tr>
                 </thead>
            </table>
        </div>
    </div>
   <!--  <div class="map_cover"></div> -->
    <!--<script type="text/javascript" src="https://api.map8.zone/maps/js/gomp.js?key=<?php echo $mapkey;?>"></script>-->
    <script type="text/javascript">
    $('#loadingIMG').hide();
    // $("#searchform").hide();
    // $("#postresult").hide();
    var nowLongitude = 0;
    var nowLatitude = 0;
    // var mapkey = "<?php echo $mapkey;?>";
    var minyear;
    var maxyear = 10*$("#houseyear").val();

    function getyearrange()
    {
        if($("#houseyear").val()==1)
        {
            minyear =0;
        }
        else
        {
            minyear =10*($("#houseyear").val()-1)+1;
        }
    }

    $().ready(function() {
        var getArr = getUrlVars();
        console.log(getArr);
        if(getArr!=null)
        {
            if(getArr.length>0)
            {
                if(getArr['searchdetail']=='1')
                {
                    $("#topbar").show();
                    $("#bottombar").show();
                    $("#leftbar").show();
                    getdetaildata();
                }
            }
        }
        

        // getprice();
        // getLocation();
        $("#getlocation").click(function(){
            // alert("getlocation")
            getLocation();
        });

        $("#searchprice").click(function(){
            if($("#basics").val=="")
            {
                alert("請輸入關鍵字!");
            }
            else
            {
                getprice();
            }
        });

        function getprice()
        {
            getyearrange();
            $("#apartment_price_range").html("取得價格中.....");
            $("#building_price_range").html("取得價格中.....");
            $("#toutiancuo_price_range").html("取得價格中.....");
            searchprice(1,minyear,maxyear);
            searchprice(2,minyear,maxyear);
            searchprice(3,minyear,maxyear);
        }

        function getLocation() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError, getOptions);
          } else {
            alert("Geolocation is not supported by this browser.");
          }
        }
        
        function showPosition(position)
        {
            var latlon = position.coords.latitude + "," + position.coords.longitude;
            nowLongitude = position.coords.longitude;
            nowLatitude = position.coords.latitude;
            // alert(latlon);
            searchlocation(nowLatitude,nowLongitude);
            // showmap();
        }

        var getOptions = {
            //是否使用高精度设备，如GPS。默认是true
            enableHighAccuracy:true,
            //超时时间，单位毫秒，默认为0
            timeout:5000,
            //使用设置时间内的缓存数据，单位毫秒
            //默认为0，即始终请求新数据
            //如设为Infinity，则始终使用缓存数据
            maximumAge:0
         };
        // if ("geolocation" in navigator) {
        //   /* geolocation is available */
        //   navigator.geolocation.getCurrentPosition(function(position) {
        //     alert(position.coords.latitude+","+position.coords.longitude);
        //     // do_something(position.coords.latitude, position.coords.longitude);
        //   });
        // } else {
        //   /* geolocation IS NOT available */
        //   alert("nolocation");
        // }

        function showError(error) {
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert("User denied the request for Geolocation.");
              break;
            case error.POSITION_UNAVAILABLE:
              alert("Location information is unavailable.");
              break;
            case error.TIMEOUT:
              alert("The request to get user location timed out.");
              break;
            case error.UNKNOWN_ERROR:
              alert("An unknown error occurred.");
              break;
          }
        }

        var mapdata = {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "type": "Feature",
                                "properties": {
                                                "description": "站點名稱：<strong>南海店</strong><br />站點編號：NH1001<br />開放時間：08:00 ~ 21:00<br />營業狀態：<span class=\"available\">營業中</span>",
                                                "image": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAqCAYAAACgLjskAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAjlJREFUeNrUl8FOwkAQhocVjAYJmBgveCDxBTTqwZN9A/EN8Amsid7x7EWfQHgC9A3grInwACZcuKiJEkI0ouBOnZJa2t2Bbk38k2VLd7ZfZ3d2Z5sYjUag0vbpc05WJVksKlmfSVeWOpXK3fnKq+p5iTAggcqyHMF0usR+YeBAoIQV8W0DvOEKvS5J6LW/QQTAbFnVIsCA+tbks0pKD8ngCszqUHpamQDSMNYgHh24w+sAKUDaEYdRN6cFDCR3Du0YYe6c2o6HWydPLO+Ggzd4f3mAz/4jjIaDn/kQKUimV2FheR1EapHlZVL+FHWwr48e9Du3Y5Ar/D/odZyXSOd3YG4+o/OyKAio9CwI5gejDdpq5AAtlQUOowrmhaKtRpbQDScOF1cM26zgvDlXHFuhmz/TUgIx1DH0ueLYaocU1xlXHFsENlQGuKg5b442aKtRQ1CmVg4rLmoVFNvQhrHbXAtKtErhDrK0tgupTP4XGK/xHrZpdpkx0M0W6OUexKuGzBaWN1vELXscpZLclNVZjLBjYkwcMTAr7xuG3UhYMWwd4pmmZRDWomcGL3w6S1qGoPgMy38+ndhpDEEDYaFbW0RoKEy5l84IVcK0m/eUUC1M+THjO/5vyOpeY7bprrVI6cmzMVRVX0wcGBtIKivaLoxkfJ+X7RAvq9QGpj100gvzXrSg8QWQt0NXepczdogKy2ue67rRU1uIvJDmXwCb/wo4ddBQ4LRpqRSm7ZucMf3UZ+wH3wIMAPnJ+VlcHpUWAAAAAElFTkSuQmCC"
                                            },
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [
                                        121.21677670309548,
                                        24.96335528543689
                                    ]
                                }
                            }]
                        };
        console.log(mapdata['features'][0]['geometry']['coordinates']);
        $.each(mapdata.features,function(i,item){
            console.log(item.geometry.coordinates);
        });
        // getLocation();
        // function getLocation() {//取得 經緯度
        //     if (navigator.geolocation) {//
        //         navigator.geolocation.getCurrentPosition(showPosition);//有拿到位置就呼叫 showPosition 函式
        //     } else { 
        //         m.innerHTML = "您的瀏覽器不支援 顯示地理位置 API ，請使用其它瀏覽器開啟 這個網址";
        //     }
        // }
     
        // function showPosition(position) {
        //     nowLongitude = position.coords.longitude;
        //     nowLatitude = position.coords.latitude;
        //     nowLongitude = 121.21677670309548;
        //     nowLatitude = 24.96335528543689;
        //     showmap();
        //     console.log("  緯度 (Latitude): " + position.coords.latitude +","+"經度 (Longitude): " + position.coords.longitude);
        // }

        nowLongitude = 121.14923;
        nowLatitude = 24.88485;
        var markers;
        // searchdata("",nowLatitude,nowLongitude);
        var zoom_value = 13;
        if (map != undefined) map.remove();
        var map = L.map('map', {
            center: [nowLatitude, nowLongitude],
            zoom: zoom_value
        });
        const createMakerCluster = () => {
            return $L.markerClusterGroup();
        };
        showmap();
        function showmap()
        {
            /* delete method */
            map.eachLayer(function(layer) {
                if (layer instanceof L.MarkerClusterGroup)
                {
                    map.removeLayer(layer)
                }
            })
            var markerEl = document.createElement('img');
            markerEl.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAqCAYAAACgLjskAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAjlJREFUeNrUl8FOwkAQhocVjAYJmBgveCDxBTTqwZN9A/EN8Amsid7x7EWfQHgC9A3grInwACZcuKiJEkI0ouBOnZJa2t2Bbk38k2VLd7ZfZ3d2Z5sYjUag0vbpc05WJVksKlmfSVeWOpXK3fnKq+p5iTAggcqyHMF0usR+YeBAoIQV8W0DvOEKvS5J6LW/QQTAbFnVIsCA+tbks0pKD8ngCszqUHpamQDSMNYgHh24w+sAKUDaEYdRN6cFDCR3Du0YYe6c2o6HWydPLO+Ggzd4f3mAz/4jjIaDn/kQKUimV2FheR1EapHlZVL+FHWwr48e9Du3Y5Ar/D/odZyXSOd3YG4+o/OyKAio9CwI5gejDdpq5AAtlQUOowrmhaKtRpbQDScOF1cM26zgvDlXHFuhmz/TUgIx1DH0ueLYaocU1xlXHFsENlQGuKg5b442aKtRQ1CmVg4rLmoVFNvQhrHbXAtKtErhDrK0tgupTP4XGK/xHrZpdpkx0M0W6OUexKuGzBaWN1vELXscpZLclNVZjLBjYkwcMTAr7xuG3UhYMWwd4pmmZRDWomcGL3w6S1qGoPgMy38+ndhpDEEDYaFbW0RoKEy5l84IVcK0m/eUUC1M+THjO/5vyOpeY7bprrVI6cmzMVRVX0wcGBtIKivaLoxkfJ+X7RAvq9QGpj100gvzXrSg8QWQt0NXepczdogKy2ue67rRU1uIvJDmXwCb/wo4ddBQ4LRpqRSm7ZucMf3UZ+wH3wIMAPnJ+VlcHpUWAAAAAElFTkSuQmCC';
            var houseIcon = new L.Icon({
              iconUrl: markerEl.src,
              iconSize: [25, 41],
              iconAnchor: [12, 41],
              popupAnchor: [1, -34],
              shadowSize: [41, 41]
            });
            map.setView(new L.LatLng(nowLatitude, nowLongitude), zoom_value);
            var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            var osm = new L.TileLayer(osmUrl, {minZoom: 6, maxZoom: 19});
            map.addLayer(osm);
            markers = new L.MarkerClusterGroup({
                    iconCreateFunction: function (cluster) {
                        const number = cluster.getChildCount();
                        let icon = IconLogic(number);

                        return L.divIcon({ html: number, className: icon.className
                                                , iconSize: icon.point });
                }
            });
            // let cluster = map.createMakerCluster();
            $.each(mapdata.features,function(i,item){
                console.log(item.properties.description);
                markers.addLayer(L.marker([item.geometry.coordinates[1],item.geometry.coordinates[0]], {icon: houseIcon}).bindPopup(item.properties.description));
            });
            map.addLayer(markers);
            // L.marker([51.5, -0.09], {icon: greenIcon}).addTo(map);

            var addzoom = document.getElementById("bt2_1");
            addzoom.onclick=function(){
                zoom_value++;
                map.setZoom(zoom_value)
            }

            var reducezoom = document.getElementById("bt2_2");
            reducezoom.onclick=function(){
                zoom_value--;
                map.setZoom(zoom_value)
            }
        }

        function searchdata(query_data,lat,lng)
        {
            var type = "";
            var features = "";
            var strMsg = "";
            var param = "";
            if(query_data!="")
                param += "&addr="+query_data;
            if(lat!="" && lng!="")
                param += "&lat="+lat+"&lng="+lng;
            $.ajax({
              url: "/?route=api/buyhows/searchmap"+param,
              dataType: "json",
              beforeSend:function(){
                  $('#loadingIMG').show();
              },
              complete:function(){
                  $('#loadingIMG').hide();
              },
              error: function(xhr) {
                  strMsg += 'Ajax request發生錯誤:'+xhr+'\n請重試';
              },
              success: function (rs) {
                $.each(rs.data,function(i,item){
                    if(Math.ceil(item.lng)!=0 && Math.ceil(item.lat)!=0)
                    {
                        if(features=="")
                        {
                            nowLongitude =item.lng;
                            nowLatitude = item.lat;
                            features = '[{'
                                            +'"type": "Feature",'
                                            +'"properties": {"description": "<strong>'+item.tw_city+item.tw_city_area+'</strong><br />地址：'+item.address+'<br />價格：'+item.total_price+'<br />"},'
                                            +'"geometry": {'
                                                +'"type": "Point",'
                                                +'"coordinates": ['
                                                    +item.lng+','
                                                    +item.lat
                                                +']'
                                            +'}'
                                        +'}';
                        }
                        else
                        {
                            features += ',{'
                                            +'"type": "Feature",'
                                            +'"properties": {"description": "<strong>'+item.tw_city+item.tw_city_area+'</strong><br />地址：'+item.address+'<br />價格：'+item.total_price+'<br />"},'
                                            +'"geometry": {'
                                                +'"type": "Point",'
                                                +'"coordinates": ['
                                                    +item.lng+','
                                                    +item.lat
                                                +']'
                                            +'}'
                                        +'}';
                        }
                    }                });
                features = features+"]";
                mapdata = {
                    "type": "Feature",
                    "features": JSON.parse(features)
                }
                $("#searchform").hide();
                showmap();
                // $.getJSON(rs,function(data){ 
                //     $.each(data.dat,function(i,item){
                //         console.log(item);
                //         if(features=="")
                //             features = "["+JSON.stringify(item);
                //         else
                //             features += ","+JSON.stringify(item);
                //         // $.each(data,function(i,item){
                //         //     console.log(item.properties);
                //         // });
                //     });
                //     // features = features+"]";
                //     // mapdata = {
                //     //     "type": data['type'],
                //     //     "features": JSON.parse(features)
                //     // }
                //     // $("#searchform").hide();
                //     // showmap();
                // }); 
                // console.log(rs);
              }
            });
            // $.getJSON('json/buyhouse.json',function(data){ 
            //     $.each(data.features,function(i,item){
            //         console.log(item);
            //         if(item.properties!=null)
            //         {
            //             if(item.properties['description'].match(query_data)!=null)
            //             {
            //                 if(features=="")
            //                     features = "["+JSON.stringify(item);
            //                 else
            //                     features += ","+JSON.stringify(item);
            //             }     
            //         }  
            //         $.each(data,function(i,item){
            //             console.log(item.properties);
            //         });
            //     });
            //     features = features+"]";
            //     mapdata = {
            //         "type": data['type'],
            //         "features": JSON.parse(features)
            //     }
            //     $("#searchform").hide();
            //     showmap();
            // }); 
        }

        function searchlocation(lat,lng)
        {
            var type = "";
            var features = "";
            var strMsg = "";
            $.ajax({
              url: "<?php echo $apiurl;?>?route=api/buyhows/getnearbyaddress&lat="+lat+"&lng="+lng,
              dataType: "json",
              beforeSend:function(){
                  $('#loadingIMG').show();
              },
              complete:function(){
                  $('#loadingIMG').hide();
              },
              error: function(xhr) {
                  strMsg += 'Ajax request發生錯誤:'+xhr+'\n請重試';
              },
              success: function (rs) {
                  console.log(rs);
                  console.log(rs.data[0].address);
                  if(rs.count==0)
                  {
                    // var jsondata = $.get("https://api.map8.zone/v2/place/geocode/json?key="+mapkey+"&latlng="+lat+","+lng);
                    // $("#basics").val(jsondata.results[0].formatted_address);
                  }
                  else
                  {
                    $("#basics").val(rs.data[0].address);
                  } 
              }
            });
        }

        function searchprice(building_type_sn,minyear,maxyear)
        {
            var type = "";
            var features = "";
            var strMsg = "";
            var address = $("#basics").val();
            $.ajax({
              url: "<?php echo $apiurl;?>?route=api/buyhows/getprice&minyear="+minyear+"&maxyear="+maxyear+"&building_type_sn="+building_type_sn+"&addr="+address,
              dataType: "json",
              beforeSend:function(){
                  $('#loadingIMG').show();
              },
              complete:function(){
                  $('#loadingIMG').hide();
              },
              error: function(xhr) {
                  strMsg += 'Ajax request發生錯誤:'+xhr+'\n請重試';
              },
              success: function (rs) {
                  console.log(rs);
                  var min_price = 0;
                  var max_price = 0;
                  if(rs.data[0].min_price!=null)
                    min_price = rs.data[0].min_price;
                  if(rs.data[0].max_price!=null)
                    max_price = rs.data[0].max_price;
                  switch(building_type_sn)
                  {
                    case 1:
                        $("#apartment_price_range").html(min_price+"~"+max_price+"萬");
                        break;
                    case 2:
                        $("#building_price_range").html(min_price+"~"+max_price+"萬");
                        break;
                    case 3:
                        $("#toutiancuo_price_range").html(min_price+"~"+max_price+"萬");
                        break;
                  }
              }
            });
        }

        $("#bt2_4").click(function(){
            $("#searchform").show();
        });

        $("#search_btn").click(function function_name (argument) {
            if($("#basics").val()=="")
            {
                alert("請先輸入關鍵字!");
            }
            else
            {
                searchdata($("#basics").val(),"","");
            } 
        });

        var options = {
            url:function(phrase) {
                return "<?php echo $apiurl;?>?route=api/buyhows/search&addr="+phrase;
            },
            // data: ["blue", "green", "pink", "red", "yellow"],
            // url: "json/buyhouse.json",
            listLocation: "data",
            getValue: "address",
            theme: "dark",
            list: {
                match: {
                    enabled: true
                },
                maxNumberOfElements: 5,
            }
        };

        // $("#basics").easyAutocomplete(options);

        // $('#basics').combobox({
        //     url:"json/buyhouse.json",
        //     valueField:'locationname',
        //     textField:'text'
        // });

        function searchdata2()
        {
            getyearrange();
            var type = "";
            var features = "";
            var strMsg = "";
            var param = "";
            $.ajax({
              url: "<?php echo $apiurl;?>?route=api/buyhows/getdetail&minyear="+minyear+"&maxyear="+maxyear+"&building_type_sn=<?php echo $_SESSION['building_type'];?>&addr=<?php echo $_SESSION['searchkey'];?>&lat=<?php echo $_SESSION['lat'];?>&lng=<?php echo $_SESSION['lng'];?>",
              dataType: "json",
              beforeSend:function(){
                  $('#loadingIMG').show();
              },
              complete:function(){
                  $('#loadingIMG').hide();
              },
              error: function(xhr) {
                  strMsg += 'Ajax request發生錯誤:'+xhr+'\n請重試';
              },
              success: function (rs) {
                $.each(rs.data,function(i,item){
                    if(Math.ceil(item.lng)!=0 && Math.ceil(item.lat)!=0)
                    {
                        if(features=="")
                        {
                            nowLongitude =item.lng;
                            nowLatitude = item.lat;
                            features = '[{'
                                            +'"type": "Feature",'
                                            +'"properties": {"description": "<strong>'+item.tw_city+item.tw_city_area+'</strong><br />地址：'+item.address+'<br />價格：'+item.total_price+'<br />"},'
                                            +'"geometry": {'
                                                +'"type": "Point",'
                                                +'"coordinates": ['
                                                    +item.lng+','
                                                    +item.lat
                                                +']'
                                            +'}'
                                        +'}';
                        }
                        else
                        {
                            features += ',{'
                                            +'"type": "Feature",'
                                            +'"properties": {"description": "<strong>'+item.tw_city+item.tw_city_area+'</strong><br />地址：'+item.address+'<br />價格：'+item.total_price+'<br />"},'
                                            +'"geometry": {'
                                                +'"type": "Point",'
                                                +'"coordinates": ['
                                                    +item.lng+','
                                                    +item.lat
                                                +']'
                                            +'}'
                                        +'}';
                        }
                    }                });
                features = features+"]";
                mapdata = {
                    "type": "Feature",
                    "features": JSON.parse(features)
                }
                $("#searchform").hide();
                showmap();
              }
            });
        }

        <?php 
            if($_SESSION['building_type']!="")
            {
                // echo "searchdata2();";
            }
        ?>
    });
    
    $("#apartment_result").click(function(){
        submitresult(1);
    });

    $("#building_result").click(function(){
        submitresult(2);
    });

    $("#toutiancuo_result").click(function(){
        submitresult(3);
    });

    function submitresult(building_type)
    {
        $("#building_type").val(building_type);
        $("#yeartype").val($("#houseyear").val());
        $("#searchkey").val($("#basics").val());
        $("#postform").submit();
    }

    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        console.log(hashes.length);
        if(hashes.length>0)
        {
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
        }
        else
        {
            vars = null;
        }
        console.log(vars);
        return vars;
    }

    function IconLogic(number) {  // 數量
        let className = 'cluster';
        let point;

        if (number < 100) {
            className += ' cluster-green';
            point = L.point(40, 40);
        } else if (number < 200) {
            className += ' cluster-yellow';
            point = L.point(60, 60);
        } else {
            className += ' cluster-red';
            point = L.point(80, 80);
        }

        return {
            className: className,
            point: point
        }
    }

    // getdetaildata();
    function getdetaildata(){
        $("#detaildata").html("<table><tr><td align='center'>資料搜尋中,請稍候.......</td></tr></table>");
        var minyear;
        var maxyear = 10*$("#houseyear").val();
        if($("#houseyear").val()==1)
        {
            minyear =0;
        }
        else
        {
            minyear =10*($("#houseyear").val()-1)+1;
        }
        $.ajax({
            url: "<?php echo $apiurl;?>?route=api/buyhows/getdetail&minyear="+minyear+"&maxyear="+maxyear+"&building_type_sn=<?php echo $_SESSION['building_type'];?>&addr=<?php echo $_SESSION['searchkey'];?>&lat=<?php echo $_SESSION['lat'];?>&lng=<?php echo $_SESSION['lng'];?>",
            dataType: "json",
            beforeSend:function(){
                $('#loadingIMG').show();
            },
            complete:function(){
                $('#loadingIMG').hide();
            },
            error: function(xhr) {
                strMsg += 'Ajax request發生錯誤:'+xhr+'\n請重試';
            },
            success: function (rs) {
                $("#result_count").html("共 "+rs.count+" 筆資料");
                var table = '<thead>'
                                +'<tr>'
                                +'<th scope="col">序號</th>'
                                +'<th scope="col">縣市</th>'
                                +'<th scope="col">區</th>'
                                +'<th scope="col">交易標的</th>'
                                +'<th scope="col">格局</th>'
                                +'<th scope="col">類型</th>'
                                +'<th scope="col">完工日期</th>'
                                +'<th scope="col">總價</th>'
                                +'</tr>'
                            +'</thead>';
                if(rs.count>0)
                {
                    table += '<tbody>'
                    $.each(rs.data,function(i,item){
                        var n = i+1;
                        table += '<tr><th scope="row">'+n+'</th>'
                                    +'<td>'+item.tw_city+'</td>'
                                    +'<td>'+item.tw_city_area+'</td>'
                                    +'<td>'+item.buy_type+'</td>'
                                    +'<td>'+item.jiaoyi_items+'</td>'
                                    +'<td>'+item.building_type+'</td>'
                                    +'<td>'+item.completed_date+'</td>'
                                    +'<td>'+item.total_price+'</td></tr>';
                    });
                    table += '</tbody>';
                }
                $("#detaildata").html(table);

            }
        });
    }
    </script>
</body>
</html>