<?php session_start();
    $mapkey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ2aWV3aWVzMTAxOUBnbWFpbC5jb20iLCJuYW1lIjoidmlld2llczEwMTlAZ21haWwuY29tIiwiaWF0IjoxNTk3OTA4MzQ2LCJvYmplY3RzIjpbIlwvbWFwc1wvanMiLCJcL2RhdGEiLCJcL3N0eWxlcyIsIlwvc3ByaXRlcyIsIlwvZm9udHMiXSwiZXhwIjoxNjAxNzA5OTQ2fQ.PifGcAMtO4PdIsEEWMUJMXHaR3miKASZoHAQ6Rtn3gc";
    $_SESSION['building_type'] = ($_POST['building_type']!==null)?$_POST['building_type']:$_SESSION['building_type'];
    $_SESSION['yeartype'] = ($_POST['yeartype']!==null)?$_POST['yeartype']:$_SESSION['yeartype'];
    $_SESSION['searchkey'] = ($_POST['searchkey']!==null)?$_POST['searchkey']:$_SESSION['searchkey'];
    $_SESSION['lng'] = ($_POST['lng']!==null)?$_POST['lng']:$_SESSION['lng'];
    $_SESSION['lat'] = ($_POST['lat']!==null)?$_POST['lat']:$_SESSION['lat'];
    switch($_SESSION['building_type'])
    {
        case 1:
            $building_type = "公寓";
            break;
        case 2:
            $building_type = "大樓";
            break;
        case 3:
            $building_type = "透天";
            break;
    }
	
    for($i=1;$i<=5;$i++)
    {
        ${'select'.$i} = "";
    }

	${'select'.$_SESSION['yeartype']} = 'selected = "selected"';

    $apiurl = "https://bs.anguskh.com/";
    // $apiurl = "http://127.0.0.1/buyhows/";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>買好室｜實價神器</title>
    <!-- <link rel="stylesheet" href="https://api.map8.zone/css/gomp.css?key=<?php echo $mapkey;?>" /> -->
    <script type='text/javascript' src='js/jquery-1.11.2.min.js'></script>
    <link href="css/fontawesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <style>
        #detaildiv{
            height: 300px;
            overflow-y:scroll; 
        }
    </style>
</head>
<body>
    <header>
        <img src="http://buyhows.com/wp-content/uploads/2020/05/%E6%9A%AB%E6%99%82LOGOR4.png" alt=""><span>實價神器</span>
    </header>

    <div class="result">
        <div class="statement">
            <select name="" id="">
                <option value="">即時定位</option>
            </select>
            <span><?php echo $building_type;?></span>
            <select name="houseyear" id="houseyear">
                <option value="1" <?php echo $select1;?>>0 ~ 10</option>
                <option value="2" <?php echo $select2;?>>11 ~ 20</option>
                <option value="3" <?php echo $select3;?>>21 ~ 30</option>
                <option value="4" <?php echo $select4;?>>31 ~ 40</option>
                <option value="5" <?php echo $select5;?>>41 ~ 50</option>
            </select>
        </div>
        <div id="chart"></div>
        <div class="table-responsive" id="detaildiv" >
            <table class="table" id="detaildata">
                <thead>
                    <tr>
                        <th scope="col">序號</th>
                        <th scope="col">縣市</th>
                        <th scope="col">區</th>
                        <th scope="col">交易標的</th>
                        <th scope="col">格局</th>
                        <th scope="col">類型</th>
                        <th scope="col">完工日期</th>
                        <th scope="col">總價</th>
                    </tr>
                 </thead>
            </table>
        </div>
        <p class="result_count" id="result_count">共 0 筆資料</p>
        <div class="result_tool">
            <a href="#">綜合評分</a>
            <a href="#">
                <i class="fas fa-ellipsis-v"></i>
                鄰近新案</a>
            <a href="./map_new.php?searchdetail=1">MAP GUI</a>
        </div>
    </div>
    <div id="postresult" style="display:none">
        <form id="postform" action="map.php" method="post">
            <input id="building_type" name="building_type" value=<?php echo $_SESSION['building_type'];?>>
        </form>
    </div>
    <script>
    window.Promise ||
      document.write(
        '<script src="js/dist/polyfill.min.js"><\/script>'
      )
    window.Promise ||
      document.write(
        '<script src="js/classList.min.js"><\/script>'
      )
    window.Promise ||
      document.write(
        '<script src="js/findindex_polyfill_mdn.js"><\/script>'
      )
    </script>
    <script src="js/apexcharts.js"></script>
    <script>
        var options = {
            chart: {
                type: 'pie',
                
            },
            stroke: {
                show: false
            },
            series: [],
            labels: [],  
            fill: {
                colors: ['#ff4560', '#00e396', '#008ffb', '#feb019']
            },
            legend: {
                position: 'bottom',
                labels: {
                    fontSize: '16px',
                    colors: '#fff',
                    useSeriesColors: false
                },
                markers: {
                    fillColors: ['#ff4560', '#00e396', '#008ffb', '#feb019']
                }
            },
            responsive: [{
                breakpoint: 735,
                options: {
                    
                }
            }],
            plotOptions: {
                pie: {
                    customScale: 1
                }
            },
            dataLabels: {
                enabled: true,
                enabledOnSeries: undefined,
                formatter: function (val, opts) {
                    return opts.w.config.series[opts.seriesIndex] + '筆'
                },
                textAnchor: 'middle',
                distributed: false,
                offsetX: 0,
                offsetY: 0,
                style: {
                    fontSize: '14px',
                    fontFamily: 'Helvetica, Arial, sans-serif',
                    fontWeight: 'bold',
                    colors: ['#ff4560', '#00e396', '#008ffb', '#feb019']
                },
                background: {
                    enabled: true,
                    foreColor: '#fff',
                    padding: 4,
                    borderRadius: 2,
                    borderWidth: 1,
                    borderColor: '#fff',
                    opacity: 0.9,
                    dropShadow: {
                    enabled: false,
                    top: 1,
                    left: 1,
                    blur: 1,
                    color: '#000',
                    opacity: 0.45,
                    }
                },
                dropShadow: {
                    enabled: false,
                },                
            },
            tooltip: {
                enabled: false
            },
        };
        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();

        getchartdata();
		var color = ['#ff4560', '#00e396', '#008ffb', '#feb019','#BF0060','#AE00AE','#8600FF','#4A4AFF','#2894FF','#00E3E3','#02F78E','#00EC00','#73BF00','#A6A600','#D9B300','#FF8000','#F75000','#984B4B','#949449','#408080','#5A5AAD','#9F4D95','#28004D','#000079'];
        function getchartdata()
        {
            var minyear;
            var maxyear = 10*$("#houseyear").val();
            if($("#houseyear").val()==1)
            {
                minyear =0;
            }
            else
            {
                minyear =10*($("#houseyear").val()-1)+1;
            }
            var strMsg="";
            $.ajax({
              url: "<?php echo $apiurl;?>?route=api/buyhows/getpricerange&minyear="+minyear+"&maxyear="+maxyear+"&building_type_sn=<?php echo $_SESSION['building_type'];?>&address=<?php echo $_SESSION['searchkey'];?>",
              dataType: "json",
              beforeSend:function(){
                  $('#loadingIMG').show();
              },
              complete:function(){
                  $('#loadingIMG').hide();
              },
              error: function(xhr) {
                  strMsg += 'Ajax request發生錯誤:'+xhr+'\n請重試';
              },
              success: function (rs) {
                  console.log(rs);
                  var series_data = new Array();
                  var labels_data = new Array();
				  var chartcolor = new Array();
                  var total_count = 0;
				  if(rs.count != 0)
				  {
					  $.each(rs.data,function(i,item){
						series_data[i] = parseInt(item.count_num);
						labels_data[i] = (parseInt(item.price_square)*10)+"~"+(parseInt(item.price_square)*10+9)+"萬";
						if(color[i]!=null)
							chartcolor[i] = color[i];
					  });
					  chart.updateOptions({
							series: series_data,
							labels: labels_data, 
							fill: {
								colors: chartcolor
							},
							legend: {
								position: 'bottom',
								labels: {
									fontSize: '16px',
									colors: '#fff',
									useSeriesColors: false
								},
								markers: {
									fillColors: chartcolor
								}
							},
					  })
				  }
				  else
				  {
					  alert("no data!");
				  }
              }
            });
        }
        
		$("#houseyear").change(function(){
			getchartdata();
            getdetaildata();
		});

        getdetaildata();
        function getdetaildata(){
            $("#detaildata").html("<table><tr><td align='center'>資料搜尋中,請稍候.......</td></tr></table>");
            var minyear;
            var maxyear = 10*$("#houseyear").val();
            if($("#houseyear").val()==1)
            {
                minyear =0;
            }
            else
            {
                minyear =10*($("#houseyear").val()-1)+1;
            }
            $.ajax({
                url: "<?php echo $apiurl;?>?route=api/buyhows/getdetail&minyear="+minyear+"&maxyear="+maxyear+"&building_type_sn=<?php echo $_SESSION['building_type'];?>&addr=<?php echo $_SESSION['searchkey'];?>&lat=<?php echo $_SESSION['lat'];?>&lng=<?php echo $_SESSION['lng'];?>",
                dataType: "json",
                beforeSend:function(){
                    $('#loadingIMG').show();
                },
                complete:function(){
                    $('#loadingIMG').hide();
                },
                error: function(xhr) {
                    strMsg += 'Ajax request發生錯誤:'+xhr+'\n請重試';
                },
                success: function (rs) {
                    $("#result_count").html("共 "+rs.count+" 筆資料");
                    var table = '<thead>'
                                    +'<tr>'
                                    +'<th scope="col">序號</th>'
                                    +'<th scope="col">縣市</th>'
                                    +'<th scope="col">區</th>'
                                    +'<th scope="col">交易標的</th>'
                                    +'<th scope="col">格局</th>'
                                    +'<th scope="col">類型</th>'
                                    +'<th scope="col">完工日期</th>'
                                    +'<th scope="col">總價</th>'
                                    +'</tr>'
                                +'</thead>';
                    if(rs.count>0)
                    {
                        table += '<tbody>'
                        $.each(rs.data,function(i,item){
                            var n = i+1;
                            table += '<tr><th scope="row">'+n+'</th>'
                                        +'<td>'+item.tw_city+'</td>'
                                        +'<td>'+item.tw_city_area+'</td>'
                                        +'<td>'+item.buy_type+'</td>'
                                        +'<td>'+item.jiaoyi_items+'</td>'
                                        +'<td>'+item.building_type+'</td>'
                                        +'<td>'+item.completed_date+'</td>'
                                        +'<td>'+item.total_price+'</td></tr>';
                        });
                        table += '</tbody>';
                    }
                    $("#detaildata").html(table);

                }
            });
        }
    </script>
</body>
</html>