<html>
	<head>
		<title>testmap</title>
		<link rel="stylesheet" href="css/leaflet/leaflet.css" />
    	<script src="js/leaflet/leaflet.js"></script>
    	<style>
    		#map { height: 180px; }
    	</style>
	</head>
	<body>
		<div id="map" class="map_fixed"></div>
		<script type="text/javascript">
			// 建立 Leaflet 地圖
			var map = L.map('map');

			// 設定經緯度座標
			map.setView(new L.LatLng(22.992, 120.239), 12);
			
			// 設定圖資來源
			var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
			var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 16});
			map.addLayer(osm);
			
		</script>
	</body>
</hrml>