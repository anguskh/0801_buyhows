<?php
// HTTP
define('SERVER_ADDRESS',        $_SERVER['HTTP_HOST']) ;
define('PROJECT_SITE',          'buyhows') ;

define('HTTP_SERVER',           'http://'.SERVER_ADDRESS.'/');

// HTTPS
define('HTTPS_SERVER',          'http://'.SERVER_ADDRESS.'/');

// DIR
define('DIR_INITIAL',           '/www/wwwroot/'.PROJECT_SITE) ;

define('DIR_APPLICATION',       DIR_INITIAL.'/catalog/');
define('DIR_SYSTEM',            DIR_INITIAL.'/system/');
define('DIR_IMAGE',             DIR_INITIAL.'/image/');
define('DIR_LANGUAGE',          DIR_INITIAL.'/catalog/language/');
define('DIR_TEMPLATE',          DIR_INITIAL.'/catalog/view/template/');
define('DIR_CONFIG',            DIR_INITIAL.'/system/config/');
define('DIR_CACHE',             DIR_INITIAL.'/system/storage/cache/');
define('DIR_DOWNLOAD',          DIR_INITIAL.'/system/storage/download/');
define('DIR_LOGS',              DIR_INITIAL.'/system/storage/logs/');
define('DIR_MODIFICATION',      DIR_INITIAL.'/system/storage/modification/');
define('DIR_UPLOAD',            DIR_INITIAL.'/system/storage/upload/');
define('DIR_EXTRA',             DIR_INITIAL.'/extra/');

// DB
define('DB_DRIVER',     'mysqli');
define('DB_HOSTNAME',   '51.79.157.51');
define('DB_USERNAME',   'buyhouse');
define('DB_PASSWORD',   'pj_buyhouse0710');
define('DB_DATABASE',   'buyhouse');
define('DB_PORT',       '3306');
define('DB_PREFIX',     'oc_');