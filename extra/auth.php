<?php
return array(
	'1' => array( 'id' => 'menu-apart', 'icon' => 'fa-building', 'name' => '公寓大廈管理',
					 'folder' => 'buyhows', 'controller' => 'apartment', 'action' => '', 'children' => array(
	)),

	'2' => array( 'id' => 'menu-clipboard', 'icon' => 'fa-clipboard', 'name' => '實價登錄管理',
					 'folder' => 'buyhows', 'controller' => 'netprice', 'action' => '', 'children' => array(
	)),

	'3' => array( 'id' => 'menu-score', 'icon' => 'fa-award', 'name' => '綜合評分管理',
					 'folder' => 'buyhows', 'controller' => 'score', 'action' => '', 'children' => array(
	)),

	'4' => array( 'id' => 'menu-build', 'icon' => 'fa-city', 'name' => '建築類型管理',
					 'folder' => 'buyhows', 'controller' => 'building', 'action' => '', 'children' => array(
	)),

	'5' => array( 'id' => 'menu-addr', 'icon' => 'fa-compass', 'name' => '地址轉座標管理',
					 'folder' => 'buyhows', 'controller' => 'pos', 'action' => '', 'children' => array(
	)),

	'6' => array( 'id' => 'menu-city', 'icon' => 'fa-map-marked-alt', 'name' => '縣市管理',
					 'folder' => 'buyhows', 'controller' => 'twcity', 'action' => '', 'children' => array(
	)),

	'7' => array( 'id' => 'menu-log', 'icon' => 'fa-users', 'name' => '使用者log',
					 'folder' => 'buyhows', 'controller' => 'userlog', 'action' => '', 'children' => array(
	)),


	'99' => array( 'id' => 'menu-user', 'icon' => 'fa-unlock', 'name' => '權限管理', 'children' => array(
		'991' => array(	'show' => 1, 'name' => '使用者管理', 'folder' => 'user', 'controller' => 'user', 'action' => ''),
		'992' => array( 'show' => 1, 'name' => '群組帳號', 'folder' => 'user', 'controller' => 'user_permission', 'action' => ''),
	)),
);