<?php
/**
 *
 */
if (!function_exists('dump')) {
	/** * Debug Helper * * Outputs the given variable(s) with formatting and location * * @access public * @param mixed variables to be output */function dump(){ list($callee) = debug_backtrace(); $arguments = func_get_args(); $total_arguments = count($arguments); echo '<fieldset style="background: #fefefe !important; border:2px red solid; padding:5px">'; echo '<legend style="background:lightgrey; padding:5px;">'.$callee['file'].' @ line: '.$callee['line'].'</legend><pre>'; $i = 0; foreach ($arguments as $argument) { echo '<br/><strong>Debug #'.(++$i).' of '.$total_arguments.'</strong>: '; var_dump($argument); } echo "</pre>"; echo "</fieldset>";}
}


/**
 * [config description]
 * @param   [type]     $f [description]
 * @return  [type]        [description]
 * @Another Angus
 * @date    2020-08-22
 */
if (!function_exists('config')) {
	function config( $f) {
	    $file = DIR_EXTRA.$f.".php" ;
	    if(is_file($file)) {
	        return include $file ;
	    }
	    return false ;
	}
}

/**
 * @param $path
 * @param int $mode
 * @return bool
 */
if (!function_exists('mac_mkdirss')) {
	function mac_mkdirss( $path,$mode=0777)
	{
	    if (!is_dir(dirname($path))){
	        mac_mkdirss(dirname($path));
	    }
	    if(!file_exists($path)){
	        return mkdir($path,$mode);
	    }
	    return true;
	}
}

/**
 * @param $f
 * @param string $c
 * @return false|int
 */
if (!function_exists('mac_write_file')) {
	function mac_write_file( $f,$c='')
	{
	    $dir = dirname($f);
	    if(!is_dir($dir)){
	        mac_mkdirss($dir);
	    }
	    return @file_put_contents($f, $c);
	}
}

/**
 * @param $f
 * @param string $arr
 */
if (!function_exists('mac_arr2file')) {
	function mac_arr2file( $f,$arr='')
	{
	    if(is_array($arr)){
	        $con = var_export($arr,true);
	    } else{
	        $con = $arr;
	    }
	    $con = "<?php\nreturn $con;";
	    mac_write_file( DIR_EXTRA.$f, $con);
	}
}

	/**
	 * [getGUID guid產生器]
	 * @param   boolean    $mod [true:產出; false:不產出]
	 * @return  [type]          [回傳 guid : A190129D-00F7-5CAC-FDB6-76969B3EEEC8]
	 * @Another Angus
	 * @date    2017-10-23
	 */
if (!function_exists('getGUID')) {
	function getGUID( $mod = true) {
		if (function_exists('com_create_guid')){
			return com_create_guid();
		} else {
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			if ( $mod) {
				$uuid = chr(123)// "{"
					.substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12)
					.chr(125);// "}"
			} else {
				$uuid = substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12) ;
			}
		return $uuid;
		}
	}
}