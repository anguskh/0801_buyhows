<?php
/**
 * 內政部實價登錄 資料匯入
 * @Another Angus
 * @date    2020-07-11
 */
include_once('inc/function.php') ;
include_once('inc/database_inc.php') ;
define('DB_HOSTNAME',	'51.79.157.51');
define('DB_USERNAME',	'buyhouse');
define('DB_PASSWORD',	'pj_buyhouse0710');
define('DB_DATABASE',	'buyhouse');
define('DB_PORT',		'3306');
define('DB_PREFIX',		'oc_');
$dbObj = new database( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT) ;

$fileLists = initCheck( $argv);

$pattern_1  = '/([a-z])_lvr_land_a.xml/' ; // 不動產買賣
$pattern_2  = '/([a-z])_lvr_land_a_build.xml/' ; // 建物不動產買賣
$pattern_3  = '/([a-z])_lvr_land_a_land.xml/' ; // 土地不動產買賣
$pattern_4  = '/([a-z])_lvr_land_a_park.xml/' ; // 停車場不動產買賣

$pattern_5  = '/([a-z]_lvr_land_a.xml)/' ; // 預售屋買賣
$pattern_6  = '/([a-z]_lvr_land_a.xml)/' ; // 土地預售屋買賣
$pattern_7  = '/([a-z]_lvr_land_a.xml)/' ; // 停車場預售屋買賣

$pattern_8  = '/([a-z]_lvr_land_a.xml)/' ; // 不動產租賃
$pattern_9  = '/([a-z]_lvr_land_a.xml)/' ; // 建物不動產租賃
$pattern_10 = '/([a-z]_lvr_land_a.xml)/' ; // 土地不動產租賃
$pattern_11 = '/([a-z]_lvr_land_a.xml)/' ; // 停車場不動產租賃


foreach ($fileLists as $iCnt => $fileName) {
	if (preg_match( $pattern_1, $fileName, $matches)) {
		importPreOwnedHouse( $argv[1].'/'.$matches[0],$matches[1]) ;
	}

}
// echo $argv[1]."/a_lvr_land_a.xml\n" ;
// print_r( $array) ;











/**
 * [importPreOwnedHouse 不動產買賣]
 * @param   string     $fileName [description]
 * @param   string     $divsion  [行政區]
 * @return  [type]               [description]
 * @Another Angus
 * @date    2020-07-12
 */
function importPreOwnedHouse( $fileName = '', $divsion = "") {
	global $dbObj ;
	$twDivArr = config('tw_division') ;
	echo $fileName."\n" ;
	$xml  = simplexml_load_file( $fileName);
	$json = json_encode($xml);
	$data = json_decode($json,TRUE);
	// echo $twDivArr[$divsion] ;

	foreach ($data['買賣'] as $iCnt => $row) {
		$sn = $row['編號'] ;
		$SQLCmd = "SELECT sn FROM map_main where sn='{$sn}'" ;
		// echo "$SQLCmd\n" ;
		$res = $dbObj->query( $SQLCmd) ;
		/**
		stdClass Object
		(
			[num_rows]	count
			[row]		1 row
			[rows]		arrays
		)
		 */
		if ( $res->num_rows < 1) {
			$insData = [] ;
			$insData['sn']                           = $row['編號'] ;
			$insData['tw_city']						 = $twDivArr[$divsion] ;
			$insData['tw_city_area']                 = !empty($row['鄉鎮市區']) ? $row['鄉鎮市區'] : '' ;
			$insData['buy_type']                     = !empty($row['交易標的']) ? $row['交易標的'] : '' ;
			$insData['address']                      = !empty($row['土地區段位置建物區段門牌']) ? $dbObj->escape($row['土地區段位置建物區段門牌']) : '' ;
			$insData['land_turn_area']               = !empty($row['土地移轉總面積平方公尺']) ? $row['土地移轉總面積平方公尺'] : '' ;
			$insData['land_use_type']                = !empty($row['都市土地使用分區']) ? $row['都市土地使用分區'] : '' ;
			// $insData['']                          = $row['非都市土地使用分'] ;
			// $insData['']                          = $row['非都市土地使用編定'] ;
			$insData['jiaoyi_date']                  = !empty($row['交易年月日']) ? $row['交易年月日'] : '' ;
			$insData['jiaoyi_date_t']                = !empty($row['交易年月日']) ? $row['交易年月日'] : '' ;
			$insData['jiaoyi_items']                 = !empty($row['交易筆棟數']) ? $row['交易筆棟數'] : '' ;
			$insData['transfer_target']              = !empty($row['移轉層次']) ? $row['移轉層次'] : '' ;
			$insData['total_level']                  = !empty($row['總樓層數']) ? $row['總樓層數'] : '' ;
			$insData['building_type']                = !empty($row['建物型態']) ? $row['建物型態'] : '' ;
			$insData['main_use']                     = !empty($row['主要用途']) ? $row['主要用途'] : '' ;
			$insData['materials']                    = !empty($row['主要建材']) ? $row['主要建材'] : '' ;
			$insData['completed_date']               = !empty($row['建築完成年月']) ? $row['建築完成年月'] : '' ;
			$insData['transfer_total_area']          = !empty($row['建物移轉總面積平方公尺']) ? $row['建物移轉總面積平方公尺'] : '' ;
			$insData['structure_building_bedroom']   = !empty($row['建物現況格局-房']) ? $row['建物現況格局-房'] : '' ;
			$insData['structure_building_apartment'] = !empty($row['建物現況格局-廳']) ? $row['建物現況格局-廳'] : '' ;
			$insData['structure_building_bathroom']  = !empty($row['建物現況格局-衛']) ? $row['建物現況格局-衛'] : '' ;
			$insData['structure_building_part']      = !empty($row['建物現況格局-隔間']) ? $row['建物現況格局-隔間'] : '' ;
			$insData['committee']                    = !empty($row['有無管理組織']) ? $row['有無管理組織'] : '' ;
			$insData['total_price']                  = !empty($row['總價元']) ? $row['總價元'] : '' ;
			$insData['price_square']                 = !empty($row['單價元平方公尺']) ? $row['單價元平方公尺'] : '' ;
			$insData['parking_type']                 = !empty($row['車位類別']) ? $row['車位類別'] : '' ;
			$insData['parking_square']               = !empty($row['車位移轉總面積平方公尺']) ? $row['車位移轉總面積平方公尺'] : '' ;
			$insData['parking_price']                = !empty($row['車位總價元']) ? $row['車位總價元'] : '' ;
			$insData['memo']                         = !empty($row['備註']) ? $dbObj->escape($row['備註']) : '' ;

			$insCol = implode(', ', array_map(
						function ($v, $k) {
							if(is_array($v)){
								return $k.'[]='.implode('&'.$k.'[]=', $v);
							}else{
								return $k."='". $v ."'";
							}
						},
						$insData,
						array_keys($insData)
					));
			$SQLCmd = "INSERT INTO map_main SET {$insCol}\n" ;
			// echo "$SQLCmd" ;
			$dbObj->query( $SQLCmd) ;
		}
	}
}


/**
 * [initCheck description]
 * @param   [type]     $argv [description]
 * @return  [type]           [description]
 * @Another Angus
 * @date    2020-07-11
 */
function initCheck( $argv) {

	if (PHP_SAPI != "cli") {
		echo "run in cli mode" ;
		exit() ;
	}
	// print_r( $argv) ;
	if ( !isset( $argv[1])) {
		echo "請使用cli模式執行，代入目錄名稱\nphp import_land.php 'folder name'\n" ;
		exit() ;
	}
	if ( !is_dir( $argv[1])) {
		echo "目錄名稱不存\n" ;
		exit() ;
	}

	$retArr = [] ;
	$lists  = scandir($argv[1]);

	foreach ($lists as $iCnt => $fileName) {
		if ( $fileName != '.' && $fileName != '..') {
			$retArr[] = $fileName ;
		}
	}

	return $retArr ;
}