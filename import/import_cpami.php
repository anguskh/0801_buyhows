<?php
/**
 * 全國建管系統 資料匯入
 * @Another Angus
 * @date    2020-08-05
 */
include_once('inc/function.php') ;
include_once('inc/database_inc.php') ;
define('DB_HOSTNAME',	'51.79.157.51');
define('DB_USERNAME',	'buyhouse');
define('DB_PASSWORD',	'pj_buyhouse0710');
define('DB_DATABASE',	'buyhouse');
define('DB_PORT',		'3306');
define('DB_PREFIX',		'oc_');
$dbObj = new database( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT) ;

// $urlStr = "http://building.tycg.gov.tw/opendata/OpenDataSearchUrl.do?d=OPENDATA&c=APM&Start=1&成立核准日期=085" ;
// $patternUrlStr = "http://building.tycg.gov.tw/opendata/OpenDataSearchUrl.do?d=OPENDATA&c=APM&Start=%s&成立核准日期=%s" ;
//	$year = str_pad($i,3,'0',STR_PAD_LEFT);
	// $year = "085" ;
	// $page = 1 ;
	// $urlStr = sprintf( $patternStr, $page, $year) ;
	// echo $urlStr."\n" ;
	// collection ( $urlStr) ;

//}
$year = 107 ;
$page = 1 ;

collection($dbObj, $year, $page) ;


/**
 * [collection description]
 * @param   string     $urlStr [description]
 * @return  [type]             [description]
 * @Another Angus
 * @date    2020-08-05
 */
function collection( $dbObj, $year='', $page='' ) {
	unset( $ch) ;
	unset( $output) ;
	unset( $info) ;
	unset( $insData) ;


	$patternUrlStr = "http://building.tycg.gov.tw/opendata/OpenDataSearchUrl.do?d=OPENDATA&c=APM&Start=%s&成立核准日期=%s" ;
	$yearStr       = str_pad($year, 3, '0', STR_PAD_LEFT) ;
	$urlStr        = sprintf( $patternUrlStr, $page, $yearStr) ;
	$pattern       = '/([0-9]*)年([0-9]*)月([0-9]*)日/' ;
	// echo "function collection"."\n" ;
	echo $urlStr."\n" ;
	$ch = curl_init();

	//设置选项，包括URL
	curl_setopt($ch, CURLOPT_URL, $urlStr);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	// //curl_setopt($ch, CURLOPT_REFERER, 'https://movie.douban.com/cinema/nowplaying/xian/');
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36');

	$output = curl_exec($ch) ;
	$info = json_decode( $output, true) ;
	// print_r( $info['data']) ;
	if ( empty( $info['data'])) {
		$year++ ;
		collection( $dbObj, $year, 1) ;
	}
	else $page += 100 ;

	// echo "id : ". $info['data'][0]['_id']['$oid'] ."\n" ;
	// echo "管理委員會名稱 : ". $info['data'][0]['管理委員會名稱'] ."\n" ;
	// echo "成立核准日期 : ". $info['data'][0]['成立核准日期'] ."\n" ;
	// // preg_match( $pattern, $info['data'][0]['成立核准日期'], $matches) ;
	// // print_r( $matches) ;
	// echo "改選會議日期 : ". $info['data'][0]['改選會議日期'] ."\n" ;
	// // preg_match( $pattern, $info['data'][0]['改選會議日期'], $matches) ;
	// // print_r( $matches) ;
	// echo "主任委員 : ". $info['data'][0]['主任委員'] ."\n" ;
	// echo "地址 : ". $info['data'][0]['地址'] ."\n" ;
	// echo "縣市別 : ". $info['data'][0]['縣市別'] ."\n" ;

	foreach ($info['data'] as $iCnt => $row) {
		echo "id : ". $row['_id']['$oid'] ."\n" ;
		echo "管理委員會名稱 : ". $row['管理委員會名稱'] ."\n" ;
		$insData['id']             = $row['_id']['$oid'] ;
		$insData['mag_committee']  = $dbObj->escape($row['管理委員會名稱']) ;
		save_log( $insData, 'bmis') ;
		preg_match( $pattern, $row['成立核准日期'], $matches) ;
		$ad_year = 1911 + intval($matches[1]) ;
		$insData['approve_date']   = "$ad_year-$matches[2]-$matches[3]" ;
		$insData['approve_year_c'] = intval($matches[1]) ;
		$insData['approve_year']   = $ad_year ;
		$insData['approve_month']  = $matches[2] ;
		$insData['approve_day']    = $matches[3] ;

		preg_match( $pattern, $row['改選會議日期'], $matches) ;
		$ad_year = 1911 + intval($matches[1]) ;
		$insData['reele_date']     = "$ad_year-$matches[2]-$matches[3]" ;
		$insData['reele_year_c']   = intval($matches[1]) ;
		$insData['reele_year']     = $ad_year ;
		$insData['reele_month']    = $matches[2] ;
		$insData['reele_day']      = $matches[3] ;

		$insData['chairman']       = $row['主任委員'] ;
		$insData['address']        = $row['地址'] ;
		$insData['city']           = $row['縣市別'] ;


		$SQLCmd = "SELECT id FROM tb_bmis where id='{$row['_id']['$oid']}'" ;
		echo "$SQLCmd\n" ;
		$res = $dbObj->query( $SQLCmd) ;
		/**
		stdClass Object
		(
			[num_rows]	count
			[row]		1 row
			[rows]		arrays
		)
		 */
		if ( $res->num_rows < 1) {
			$SQLCmd = 'INSERT INTO tb_bmis SET '. buildInsSqlStr( $insData) ;
			// save_log( $SQLCmd, 'bmis') ;
			$dbObj->query( $SQLCmd) ;
			unset( $insData) ;
		}
	}
	sleep( 10) ;
	collection( $dbObj, $year, $page) ;
}

/**
 * [buildInsSqlStr description]
 * @param   array      $data [description]
 * @return  [type]           [description]
 * @Another Angus
 * @date    2020-08-06
 */
function buildInsSqlStr( $data = []) {
	return implode(', ', array_map(
						function ($v, $k) {
							if(is_array($v)){
								return $k.'[]='.implode('&'.$k.'[]=', $v);
							}else{
								return $k."='". $v ."'";
							}
						},
						$data,
						array_keys($data)
					));
}

function save_log( $msg, $fileName) {
	$folder = "log" ;
	if ( !is_dir( $folder)) {
		mkdir( $folder) ;
	}
	$fileDate = date( "Y_md" ) ;
	$logFileName = "{$folder}/{$fileName}_{$fileDate}.log" ;
	$fp = fopen($logFileName , 'a' ) ;
	$nowTime = date("Y/m/d H:i:s");

	if (is_array($msg)) {
		fwrite($fp,"{$nowTime} | ".print_r($msg, true)."\r\n") ;
		// fwrite($fp,print_r($msg, true)."\r\n") ;
	} else {
		fwrite($fp,"{$nowTime} | {$msg}\r\n") ;
		// fwrite($fp,"{$msg}\r\n") ;
	}
	 fclose($fp);
}














