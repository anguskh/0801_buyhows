<?php
/**
 *
 */
error_reporting(E_ALL) ;
ini_set('display_errors','On') ;
date_default_timezone_set("Asia/Taipei") ;
ini_set("memory_limit","2048M");

/**
 * [save_log description]
 * @param   [type]     $msg      [description]
 * @param   [type]     $fileName [description]
 * @return  [type]               [description]
 * @Another Angus
 * @date    2020-08-15
 */
function save_log( $msg, $fileName) {
    $folder = "log" ;
    if ( !is_dir( $folder)) {
        mkdir( $folder) ;
    }
    $fileDate = date( "Y_md" ) ;
    $logFileName = "{$folder}/{$fileName}_{$fileDate}.log" ;
    $fp = fopen($logFileName , 'a' ) ;
    $nowTime = date("Y/m/d H:i:s");

    if (is_array($msg)) {
        fwrite($fp,"{$nowTime} | ".var_dump($msg, true)."\r\n") ;
        // fwrite($fp,print_r($msg, true)."\r\n") ;
    } else {
        fwrite($fp,"{$nowTime} | {$msg}\r\n") ;
        // fwrite($fp,"{$msg}\r\n") ;
    }
     fclose($fp);
}

/**
 * [buildSqlCol description]
 * @param   array      $data [description]
 * @return  [type]           [description]
 * @Another Angus
 * @date    2020-08-15
 */
function buildSqlCol( $data = []) {
    return implode(', ', array_map(
            function ($v, $k) {
                if(is_array($v)){
                    return $k.'[]='.implode('&'.$k.'[]=', $v);
                }else{
                    return $k."='". $v ."'";
                }
            },
            $data,
            array_keys($data)
        ));
}

function config( $f) {
    $file = "inc/extra/".$f.".php" ;
    if(is_file($file)) {
        return include $file ;
    }
    return false ;
}

/**
 * @param $path
 * @param int $mode
 * @return bool
 */
function mac_mkdirss( $path,$mode=0777)
{
    if (!is_dir(dirname($path))){
        mac_mkdirss(dirname($path));
    }
    if(!file_exists($path)){
        return mkdir($path,$mode);
    }
    return true;
}

/**
 * @param $f
 * @param string $c
 * @return false|int
 */
function mac_write_file( $f,$c='')
{
    $dir = dirname($f);
    if(!is_dir($dir)){
        mac_mkdirss($dir);
    }
    return @file_put_contents($f, $c);
}

/**
 * @param $f
 * @param string $arr
 */
function mac_arr2file( $f,$arr='')
{
    if(is_array($arr)){
        $con = var_export($arr,true);
    } else{
        $con = $arr;
    }
    $con = "<?php\nreturn $con;";
    mac_write_file($f, $con);
}

/**
 * @param $strUrl
 * @return bool|string
 */
function useCurl( $strUrl) {
    $ch = curl_init() ;
    //永遠抓最新
    $userIP = getRandIP() ;

    $header = array(
        "Connection: Keep-Alive",
        "Accept: text/html, application/xhtml+xml, */*",
        "Pragma: no-cache",
        "Cache-Control: no-cache",
        "Accept-Language: zh-Hans-CN,zh-Hans;q=0.8,en-US;q=0.5,en;q=0.3",
        "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)",
        'CLIENT-IP:'.$userIP,
        'X-FORWARDED-FOR:'.$userIP,
    );

    curl_setopt($ch, CURLOPT_URL, $strUrl);
    curl_setopt($ch, CURLOPT_HEADER, $header);

    //等待時間
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);

    $agentStr = getCurlAgent() ;
    // curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0");
    curl_setopt($ch, CURLOPT_USERAGENT, $agentStr);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_REFERER, "http://www.eventpal.com.tw");   //構造來路
    curl_setopt($ch, CURLOPT_POST, 1);
    $returnData = $agentStr."\n" ;
    $request = curl_exec($ch);
    $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE) . "\n" ;

    curl_close( $ch) ;


    return $returnData.$return_code.$request ;
}

/**
 * [usePostCurl description]
 * @param   string     $strUrl [description]
 * @param   array      $data   [description]
 * @return  [type]             [description]
 * @Another Angus
 * @date    2020-05-13
 */
function usePostCurl( $strUrl = "", $data = []) {
    $ch = curl_init() ;
    //永遠抓最新
    $userIP = getRandIP() ;

    $header = array(
        "Connection: Keep-Alive",
        "Accept: text/html, application/xhtml+xml, */*",
        "Pragma: no-cache",
        "Cache-Control: no-cache",
        "Accept-Language: zh-Hans-CN,zh-Hans;q=0.8,en-US;q=0.5,en;q=0.3",
        "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)",
        'CLIENT-IP:'.$userIP,
        'X-FORWARDED-FOR:'.$userIP,
    );

    curl_setopt($ch, CURLOPT_URL, $strUrl);
    curl_setopt($ch, CURLOPT_HEADER, $header);

    //等待時間
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);

    $agentStr = getCurlAgent() ;
    // curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0");
    curl_setopt($ch, CURLOPT_USERAGENT, $agentStr);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_REFERER, "http://www.eventpal.com.tw");   //構造來路
    curl_setopt($ch, CURLOPT_POST, 1);
    $returnData = $agentStr."\n" ;
    $request = curl_exec($ch);
    $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE) . "\n" ;

    curl_close( $ch) ;


    return $returnData.$return_code.$request ;
}

/**
 * [getCurlAgent description]
 * @return  [type]     [description]
 * @Another Angus
 * @date    2020-05-09
 */
function getCurlAgent() {
    $agentArr = config( 'agent') ;
    return array_rand($agentArr) ;
}

/**
 * [getRandIP description]
 * @return  [type]     [description]
 * @Another Angus
 * @date    2020-05-09
 */
function getRandIP() {
    $ip_long = array(
       array('607649792', '608174079'), //36.56.0.0-36.63.255.255
       array('1038614528', '1039007743'), //61.232.0.0-61.237.255.255
       array('1783627776', '1784676351'), //106.80.0.0-106.95.255.255
       array('2035023872', '2035154943'), //121.76.0.0-121.77.255.255
       array('2078801920', '2079064063'), //123.232.0.0-123.235.255.255
       array('-1950089216', '-1948778497'), //139.196.0.0-139.215.255.255
       array('-1425539072', '-1425014785'), //171.8.0.0-171.15.255.255
       array('-1236271104', '-1235419137'), //182.80.0.0-182.92.255.255
       array('-770113536', '-768606209'), //210.25.0.0-210.47.255.255
       array('-569376768', '-564133889'), //222.16.0.0-222.95.255.255
   );
   $rand_key = mt_rand(0, 9);
   return long2ip(mt_rand($ip_long[$rand_key][0], $ip_long[$rand_key][1]));
}
