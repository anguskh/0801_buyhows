<?php
return array (
  'a' => '臺北市',
  'b' => '臺中市',
  'c' => '基隆市',
  'd' => '臺南市',
  'e' => '高雄市',
  'f' => '新北市',
  'g' => '宜蘭縣',
  'h' => '桃園市',
  'i' => '嘉義市',
  'j' => '新竹縣',
  'k' => '苗栗縣',
  'l' => '',
  'm' => '南投縣',
  'n' => '彰化縣',
  'o' => '新竹市',
  'p' => '雲林縣',
  'q' => '嘉義縣',
  'r' => '',
  's' => '',
  't' => '屏東縣',
  'u' => '花蓮縣',
  'v' => '臺東縣',
  'w' => '金門縣',
  'x' => '澎湖縣',
  'y' => '',
  'z' => '連江縣',
);