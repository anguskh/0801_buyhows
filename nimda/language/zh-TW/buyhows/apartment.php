<?php
// Heading
$_['heading_title']     = '公寓大廈管理';

// Text
$_['text_success']      = '更新成功';
$_['text_list']         = '列表';
$_['text_add']          = '新增';
$_['text_edit']         = '編輯';

// Column
$_['column_mag_committee']  = '管理委員會名稱';
$_['column_approve_date']   = '成立核准日期';
$_['column_approve_day']  = 'approve_day';
$_['column_reele_date']     = '改選會議日期';
$_['column_reele_day'] = 'reele_day';
$_['column_chairman'] = '主任委員';
$_['column_address'] = '地址';
$_['column_city'] = '縣市別';
$_['column_action']     = '動作';


// Entry
$_['entry_mag_committee']  = '管理委員會名稱';
$_['entry_approve_date']   = '成立核准日期';
$_['entry_approve_year_c']   = 'approve_year_c';
$_['entry_approve_year']   = 'approve_year';
$_['entry_approve_month']   = 'approve_month';
$_['entry_approve_day']  = 'approve_day';
$_['entry_reele_date']     = '改選會議日期';
$_['entry_reele_year_c']     = 'reele_year_c';
$_['entry_reele_year']     = 'reele_year';
$_['entry_reele_month']     = 'reele_month';
$_['entry_reele_day'] = 'reele_day';
$_['entry_chairman'] = '主任委員';
$_['entry_address'] = '地址';
$_['entry_city'] = '縣市別';


// Error
$_['error_mag_committee'] 	= '管理委員會名稱不得空白!';
