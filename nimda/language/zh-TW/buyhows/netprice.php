<?php
// Heading
$_['heading_title']     = '實價登錄管理';

// Text
$_['text_success']      = '更新成功';
$_['text_list']         = '列表';
$_['text_add']          = '新增';
$_['text_edit']         = '編輯';

// Column
$_['column_tw_city']  = '城市';
$_['column_tw_city_area']   = '區域';
$_['column_buy_type']  = '交易標的';
$_['column_address']     = '土地區段位置建物區段門牌';
$_['column_land_turn_area'] = '土地移轉總面積平方公尺';
$_['column_land_use_type'] = '都市土地使用分區';
$_['column_land_use_type_none'] = '非都市土地使用分區';
$_['column_land_use_type_none_code'] = '非都市土地使用編定';
$_['column_jiaoyi_date'] = '交易年月日';
$_['column_transfer_target'] = '交易筆棟數';
$_['column_land_use_type_none_code'] = '移轉層次';
$_['column_total_level'] = '總樓層數';
$_['column_building_type'] = '建物型態';
$_['column_main_use'] = '主要用途';
$_['column_materials'] = '主要建材';
$_['column_completed_date'] = '建築完成年月';
$_['column_jiaoyi_date_t'] = '交易年月日';
$_['column_transfer_total_area'] = '建物移轉總面積平方公尺';
$_['column_structure_building_bedroom'] = '建物現況格局-房';
$_['column_structure_building_apartment'] = '建物現況格局-廳';
$_['column_structure_building_bathroom'] = '建物現況格局-衛';
$_['column_structure_building_part'] = '建物現況格局-隔間';
$_['column_committee'] = '有無管理組織';
$_['column_total_price'] = '總價元';
$_['column_price_square'] = '單價元平方公尺';
$_['column_parking_type'] = '車位類別';
$_['column_parking_square'] = '車位移轉總面積平方公尺';
$_['column_parking_price'] = '車位總價元';
$_['column_memo'] = '備註';
$_['column_action'] = '動作';


// Entry
$_['entry_tw_city']  = '城市';
$_['entry_tw_city_area']   = '區域';
$_['entry_buy_type']  = '交易標的';
$_['entry_address']     = '土地區段位置建物區段門牌';
$_['entry_land_turn_area'] = '土地移轉總面積平方公尺';
$_['entry_land_use_type'] = '都市土地使用分區';
$_['entry_land_use_type_none'] = '非都市土地使用分區';
$_['entry_land_use_type_none_code'] = '非都市土地使用編定';
$_['entry_jiaoyi_date'] = '交易年月日';
$_['entry_transfer_target'] = '交易筆棟數';
$_['entry_land_use_type_none_code'] = '移轉層次';
$_['entry_total_level'] = '總樓層數';
$_['entry_building_type'] = '建物型態';
$_['entry_main_use'] = '主要用途';
$_['entry_materials'] = '主要建材';
$_['entry_completed_date'] = '建築完成年月';
$_['entry_jiaoyi_date_t'] = '交易年月日';
$_['entry_transfer_total_area'] = '建物移轉總面積平方公尺';
$_['entry_structure_building_bedroom'] = '建物現況格局-房';
$_['entry_structure_building_apartment'] = '建物現況格局-廳';
$_['entry_structure_building_bathroom'] = '建物現況格局-衛';
$_['entry_structure_building_part'] = '建物現況格局-隔間';
$_['entry_committee'] = '有無管理組織';
$_['entry_total_price'] = '總價元';
$_['entry_price_square'] = '單價元平方公尺';
$_['entry_parking_type'] = '車位類別';
$_['entry_parking_square'] = '車位移轉總面積平方公尺';
$_['entry_parking_price'] = '車位總價元';
$_['entry_memo'] = '備註';


// Error
//$_['error_mag_committee'] 	= '管理委員會名稱不得空白!';
