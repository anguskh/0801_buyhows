<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-user" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_mag_committee; ?></label>
            <div class="col-sm-10">
              <input type="text" name="mag_committee" value="<?php echo $mag_committee; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_approve_date; ?></label>
            <div class="col-sm-10">
              <input type="text" name="approve_date" value="<?php echo $approve_date; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_approve_year_c; ?></label>
            <div class="col-sm-10">
              <input type="text" name="approve_year_c" value="<?php echo $approve_year_c; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_approve_month; ?></label>
            <div class="col-sm-10">
              <input type="text" name="approve_month" value="<?php echo $approve_month; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_approve_day; ?></label>
            <div class="col-sm-10">
              <input type="text" name="approve_day" value="<?php echo $approve_day; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_reele_date; ?></label>
            <div class="col-sm-10">
              <input type="text" name="reele_date" value="<?php echo $reele_date; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_reele_year_c; ?></label>
            <div class="col-sm-10">
              <input type="text" name="reele_year_c" value="<?php echo $reele_year_c; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_reele_year; ?></label>
            <div class="col-sm-10">
              <input type="text" name="reele_year" value="<?php echo $reele_year; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_reele_month; ?></label>
            <div class="col-sm-10">
              <input type="text" name="reele_month" value="<?php echo $reele_month; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_reele_day; ?></label>
            <div class="col-sm-10">
              <input type="text" name="reele_day" value="<?php echo $reele_day; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_chairman; ?></label>
            <div class="col-sm-10">
              <input type="text" name="chairman" value="<?php echo $chairman; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_address; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address" value="<?php echo $address; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_city; ?></label>
            <div class="col-sm-10">
              <input type="text" name="city" value="<?php echo $city; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 