<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-user').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-user">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td width="5%" class="text-center">
                                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'mag_committee') { ?>
                                    <a href="<?php echo $sort_mag_committee; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_mag_committee; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_mag_committee; ?>"><?php echo $column_mag_committee; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'approve_date') { ?>
                                    <a href="<?php echo $sort_approve_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_approve_date; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_approve_date; ?>"><?php echo $column_approve_date; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'approve_day') { ?>
                                    <a href="<?php echo $sort_approve_day; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_approve_date; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_approve_day; ?>"><?php echo $column_approve_day; ?></a>
                                    <?php } ?>
                                </td>
								
                                <td class="text-center">
                                    <?php if ($sort == 'reele_date') { ?>
                                    <a href="<?php echo $sort_reele_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_reele_date; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_reele_date; ?>"><?php echo $column_reele_date; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'reele_day') { ?>
                                    <a href="<?php echo $sort_reele_day; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_reele_day; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_reele_day; ?>"><?php echo $column_reele_day; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'chairman') { ?>
                                    <a href="<?php echo $sort_chairman; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_chairman; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_chairman; ?>"><?php echo $column_chairman; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'address') { ?>
                                    <a href="<?php echo $sort_address; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_address; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_address; ?>"><?php echo $column_address; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'city') { ?>
                                    <a href="<?php echo $sort_city; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_address; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_city; ?>"><?php echo $column_city; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($apartments) { ?>
                            <?php foreach ($apartments as $apartment) { ?>
                            <tr>
                                <td class="text-center">
                                    <?php if (in_array($apartment['id'], $selected)) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $apartment['id']; ?>" checked="checked" />
                                    <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $apartment['id']; ?>" />
                                    <?php } ?>
                                </td>
                                <td class="text-center"><?php echo $apartment['mag_committee']; ?></td>
                                <td class="text-center"><?php echo $apartment['approve_date']; ?></td>
                                <td class="text-center"><?php echo $apartment['approve_day']; ?></td>
                                <td class="text-center"><?php echo $apartment['reele_date']; ?></td>
                                <td class="text-center"><?php echo $apartment['reele_day']; ?></td>
                                <td class="text-center"><?php echo $apartment['chairman']; ?></td>
                                <td class="text-center"><?php echo $apartment['address']; ?></td>
                                <td class="text-center"><?php echo $apartment['city']; ?></td>
                                <td class="text-center"><a href="<?php echo $apartment['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?> 