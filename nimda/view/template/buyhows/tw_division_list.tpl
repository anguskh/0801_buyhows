<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-user" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="保存"><i class="fa fa-save"></i></button>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<td width="5%" class="text-center">
								<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
							</td>
							<td class="text-center">
								縣市名稱
							</td>
						</tr>
					</thead>
					<tbody>
						<?php if ($tw_division) : ?>
                        <?php foreach ($tw_division as $colName => $tw_name) : ?>
						<tr>
							<td class="text-center">
								<h2>
									<span class="badge badge-pill badge-danger">&nbsp;- <?=$colName?> -&nbsp;</span>
								</h2>
							</td>
							<td class="text-center">
								<div class="form-group" style="padding-top: 0px;">
									<div class="col-sm-10">
										<input type="text" name="tw_anme[<?=$colName?>]" value="<?=$tw_name?>" placeholder="請填寫縣市名稱" class="form-control" >
									</div>
								</div>
							</td>
						</tr>
						<?php endforeach ; ?>
						<?php else : ?>
						<tr>
							<td colspan="2">目前尚無建立資料</td>
						</tr>

						<?php endif ; ?>
					</tbody>
				</table>
				</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>