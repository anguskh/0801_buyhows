<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-user').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-user">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td width="5%" class="text-center">
                                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'tw_city') { ?>
                                    <a href="<?php echo $sort_tw_city; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_tw_city; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_tw_city; ?>"><?php echo $column_tw_city; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'tw_city_area') { ?>
                                    <a href="<?php echo $sort_tw_city_area; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_tw_city_area; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_tw_city_area; ?>"><?php echo $column_tw_city_area; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'buy_type') { ?>
                                    <a href="<?php echo $sort_buy_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_buy_type; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_buy_type; ?>"><?php echo $column_buy_type; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'address') { ?>
                                    <a href="<?php echo $sort_address; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_address; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_address; ?>"><?php echo $column_address; ?></a>
                                    <?php } ?>
                                </td>
								
                                <td class="text-center">
                                    <?php if ($sort == 'land_turn_area') { ?>
                                    <a href="<?php echo $sort_land_turn_area; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_land_turn_area; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_land_turn_area; ?>"><?php echo $column_land_turn_area; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'land_use_type') { ?>
                                    <a href="<?php echo $sort_land_use_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_land_use_type; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_land_use_type; ?>"><?php echo $column_land_use_type; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'land_use_type_none') { ?>
                                    <a href="<?php echo $sort_land_use_type_none; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_land_use_type_none; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_land_use_type_none; ?>"><?php echo $column_land_use_type_none; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'land_use_type_none_code') { ?>
                                    <a href="<?php echo $sort_land_use_type_none_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_land_use_type_none_code; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_land_use_type_none_code; ?>"><?php echo $column_land_use_type_none_code; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'jiaoyi_date') { ?>
                                    <a href="<?php echo $sort_jiaoyi_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_jiaoyi_date; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_jiaoyi_date; ?>"><?php echo $column_jiaoyi_date; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'transfer_target') { ?>
                                    <a href="<?php echo $sort_transfer_target; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_transfer_target; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_transfer_target; ?>"><?php echo $column_transfer_target; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'total_level') { ?>
                                    <a href="<?php echo $sort_total_level; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total_level; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_total_level; ?>"><?php echo $column_total_level; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'building_type') { ?>
                                    <a href="<?php echo $sort_building_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_building_type; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_building_type; ?>"><?php echo $column_building_type; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'main_use') { ?>
                                    <a href="<?php echo $sort_main_use; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_main_use; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_main_use; ?>"><?php echo $column_main_use; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'materials') { ?>
                                    <a href="<?php echo $sort_materials; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_materials; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_materials; ?>"><?php echo $column_materials; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'completed_date') { ?>
                                    <a href="<?php echo $sort_completed_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_completed_date; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_completed_date; ?>"><?php echo $column_completed_date; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'jiaoyi_date_t') { ?>
                                    <a href="<?php echo $sort_jiaoyi_date_t; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_jiaoyi_date_t; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_jiaoyi_date_t; ?>"><?php echo $column_jiaoyi_date_t; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'transfer_total_area') { ?>
                                    <a href="<?php echo $sort_transfer_total_area; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_transfer_total_area; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_transfer_total_area; ?>"><?php echo $column_transfer_total_area; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'structure_building_bedroom') { ?>
                                    <a href="<?php echo $sort_structure_building_bedroom; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_structure_building_bedroom; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_structure_building_bedroom; ?>"><?php echo $column_structure_building_bedroom; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'structure_building_apartment') { ?>
                                    <a href="<?php echo $sort_structure_building_apartment; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_structure_building_apartment; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_structure_building_apartment; ?>"><?php echo $column_structure_building_apartment; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'structure_building_bathroom') { ?>
                                    <a href="<?php echo $sort_structure_building_bathroom; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_structure_building_bathroom; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_structure_building_bathroom; ?>"><?php echo $column_structure_building_bathroom; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'structure_building_part') { ?>
                                    <a href="<?php echo $sort_structure_building_part; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_structure_building_part; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_structure_building_part; ?>"><?php echo $column_structure_building_part; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'committee') { ?>
                                    <a href="<?php echo $sort_committee; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_committee; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_committee; ?>"><?php echo $column_committee; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'total_price') { ?>
                                    <a href="<?php echo $sort_total_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total_price; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_total_price; ?>"><?php echo $column_total_price; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'price_square') { ?>
                                    <a href="<?php echo $sort_price_square; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price_square; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_price_square; ?>"><?php echo $column_price_square; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'parking_type') { ?>
                                    <a href="<?php echo $sort_parking_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_parking_type; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_parking_type; ?>"><?php echo $column_parking_type; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'parking_square') { ?>
                                    <a href="<?php echo $sort_parking_square; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_parking_square; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_parking_square; ?>"><?php echo $column_parking_square; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'materials') { ?>
                                    <a href="<?php echo $sort_materials; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_materials; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_materials; ?>"><?php echo $column_materials; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'parking_price') { ?>
                                    <a href="<?php echo $sort_parking_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_parking_price; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_parking_price; ?>"><?php echo $column_parking_price; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($sort == 'memo') { ?>
                                    <a href="<?php echo $sort_memo; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_memo; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_memo; ?>"><?php echo $column_memo; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="text-center"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($netprices) { ?>
                            <?php foreach ($netprices as $netprice) { ?>
                            <tr>
                                <td class="text-center">
                                    <?php if (in_array($netprice['sn'], $selected)) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $netprice['sn']; ?>" checked="checked" />
                                    <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $netprice['sn']; ?>" />
                                    <?php } ?>
                                </td>
                                <td class="text-center"><?php echo $netprice['tw_city']; ?></td>
                                <td class="text-center"><?php echo $netprice['tw_city_area']; ?></td>
                                <td class="text-center"><?php echo $netprice['buy_type']; ?></td>
                                <td class="text-center"><?php echo $netprice['address']; ?></td>
                                <td class="text-center"><?php echo $netprice['land_turn_area']; ?></td>
                                <td class="text-center"><?php echo $netprice['land_use_type']; ?></td>
                                <td class="text-center"><?php echo $netprice['land_use_type_none']; ?></td>
                                <td class="text-center"><?php echo $netprice['land_use_type_none_code']; ?></td>
                                <td class="text-center"><?php echo $netprice['jiaoyi_date']; ?></td>
                                <td class="text-center"><?php echo $netprice['transfer_target']; ?></td>
                                <td class="text-center"><?php echo $netprice['land_use_type_none_code']; ?></td>
                                <td class="text-center"><?php echo $netprice['total_level']; ?></td>
                                <td class="text-center"><?php echo $netprice['building_type']; ?></td>
                                <td class="text-center"><?php echo $netprice['main_use']; ?></td>
                                <td class="text-center"><?php echo $netprice['materials']; ?></td>
                                <td class="text-center"><?php echo $netprice['completed_date']; ?></td>
                                <td class="text-center"><?php echo $netprice['jiaoyi_date_t']; ?></td>
                                <td class="text-center"><?php echo $netprice['transfer_total_area']; ?></td>
                                <td class="text-center"><?php echo $netprice['structure_building_bedroom']; ?></td>
                                <td class="text-center"><?php echo $netprice['structure_building_apartment']; ?></td>
                                <td class="text-center"><?php echo $netprice['structure_building_bathroom']; ?></td>
                                <td class="text-center"><?php echo $netprice['structure_building_part']; ?></td>
                                <td class="text-center"><?php echo $netprice['committee']; ?></td>
                                <td class="text-center"><?php echo $netprice['total_price']; ?></td>
                                <td class="text-center"><?php echo $netprice['price_square']; ?></td>
                                <td class="text-center"><?php echo $netprice['parking_type']; ?></td>
                                <td class="text-center"><?php echo $netprice['parking_square']; ?></td>
                                <td class="text-center"><?php echo $netprice['parking_price']; ?></td>
                                <td class="text-center"><?php echo $netprice['memo']; ?></td>
                                <td class="text-center"><a href="<?php echo $netprice['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?> 