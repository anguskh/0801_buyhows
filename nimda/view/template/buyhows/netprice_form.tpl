<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-user" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-tw_city"><?php echo $entry_tw_city; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tw_city" value="<?php echo $tw_city; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-tw_city_area"><?php echo $entry_tw_city_area; ?></label>
            <div class="col-sm-10">
              <input type="text" name="tw_city_area" value="<?php echo $tw_city_area; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-buy_type"><?php echo $entry_buy_type; ?></label>
            <div class="col-sm-10">
              <input type="text" name="buy_type" value="<?php echo $buy_type; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-address"><?php echo $entry_address; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address" value="<?php echo $address; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-land_turn_area"><?php echo $entry_land_turn_area; ?></label>
            <div class="col-sm-10">
              <input type="text" name="land_turn_area" value="<?php echo $land_turn_area; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-land_use_type"><?php echo $entry_land_use_type; ?></label>
            <div class="col-sm-10">
              <input type="text" name="land_use_type" value="<?php echo $land_use_type; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-land_use_type_none"><?php echo $entry_land_use_type_none; ?></label>
            <div class="col-sm-10">
              <input type="text" name="land_use_type_none" value="<?php echo $land_use_type_none; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-land_use_type_none_code"><?php echo $entry_land_use_type_none_code; ?></label>
            <div class="col-sm-10">
              <input type="text" name="land_use_type_none_code" value="<?php echo $land_use_type_none_code; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-jiaoyi_date"><?php echo $entry_jiaoyi_date; ?></label>
            <div class="col-sm-10">
              <input type="text" name="jiaoyi_date" value="<?php echo $jiaoyi_date; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-transfer_target"><?php echo $entry_transfer_target; ?></label>
            <div class="col-sm-10">
              <input type="text" name="transfer_target" value="<?php echo $transfer_target; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-total_level"><?php echo $entry_total_level; ?></label>
            <div class="col-sm-10">
              <input type="text" name="total_level" value="<?php echo $total_level; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-building_type"><?php echo $entry_building_type; ?></label>
            <div class="col-sm-10">
              <input type="text" name="building_type" value="<?php echo $building_type; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-main_use"><?php echo $entry_main_use; ?></label>
            <div class="col-sm-10">
              <input type="text" name="main_use" value="<?php echo $main_use; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-materials"><?php echo $entry_materials; ?></label>
            <div class="col-sm-10">
              <input type="text" name="materials" value="<?php echo $materials; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-completed_date"><?php echo $entry_completed_date; ?></label>
            <div class="col-sm-10">
              <input type="text" name="completed_date" value="<?php echo $completed_date; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-jiaoyi_date_t"><?php echo $entry_jiaoyi_date_t; ?></label>
            <div class="col-sm-10">
              <input type="text" name="jiaoyi_date_t" value="<?php echo $jiaoyi_date_t; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-transfer_total_area"><?php echo $entry_transfer_total_area; ?></label>
            <div class="col-sm-10">
              <input type="text" name="transfer_total_area" value="<?php echo $transfer_total_area; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-structure_building_bedroom"><?php echo $entry_structure_building_bedroom; ?></label>
            <div class="col-sm-10">
              <input type="text" name="structure_building_bedroom" value="<?php echo $structure_building_bedroom; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-structure_building_apartment"><?php echo $entry_structure_building_apartment; ?></label>
            <div class="col-sm-10">
              <input type="text" name="structure_building_apartment" value="<?php echo $structure_building_apartment; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-structure_building_bathroom"><?php echo $entry_structure_building_bathroom; ?></label>
            <div class="col-sm-10">
              <input type="text" name="structure_building_bathroom" value="<?php echo $structure_building_bathroom; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-structure_building_part"><?php echo $entry_structure_building_part; ?></label>
            <div class="col-sm-10">
              <input type="text" name="structure_building_part" value="<?php echo $structure_building_part; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-committee"><?php echo $entry_committee; ?></label>
            <div class="col-sm-10">
              <input type="text" name="committee" value="<?php echo $committee; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-total_price"><?php echo $entry_total_price; ?></label>
            <div class="col-sm-10">
              <input type="text" name="total_price" value="<?php echo $total_price; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-price_square"><?php echo $entry_price_square; ?></label>
            <div class="col-sm-10">
              <input type="text" name="price_square" value="<?php echo $price_square; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-parking_type"><?php echo $entry_parking_type; ?></label>
            <div class="col-sm-10">
              <input type="text" name="parking_type" value="<?php echo $parking_type; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-parking_square"><?php echo $entry_parking_square; ?></label>
            <div class="col-sm-10">
              <input type="text" name="parking_square" value="<?php echo $parking_square; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-parking_price"><?php echo $entry_parking_price; ?></label>
            <div class="col-sm-10">
              <input type="text" name="parking_price" value="<?php echo $parking_price; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-memo"><?php echo $entry_memo; ?></label>
            <div class="col-sm-10">
              <input type="text" name="memo" value="<?php echo $memo; ?>" placeholder="" id="input-committee" class="form-control" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 