<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_total_list; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-center">&nbsp;</td>
                                        <td class="text-center"><?php echo $column_onsale; ?></td>
                                        <td class="text-center"><?php echo $column_offsale; ?></td>
                                        <td class="text-center"><?php echo $column_total; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center"><?php echo $column_album; ?></td>
                                        <td class="text-center"><?php echo $total_albumON; ?></td>
                                        <td class="text-center"><?php echo $total_albumOFF; ?></td>
                                        <td class="text-center"><?php echo ($total_albumON+$total_albumOFF); ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><?php echo $column_product; ?></td>
                                        <td class="text-center"><?php echo $total_productON; ?></td>
                                        <td class="text-center"><?php echo $total_productOFF; ?></td>
                                        <td class="text-center"><?php echo ($total_productON+$total_productOFF); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_create_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-center" rowspan="2"><?php echo $column_month; ?></td>
                                <td class="text-center" colspan="3"><?php echo $column_album; ?></td>
                                <td class="text-center" colspan="3"><?php echo $column_product; ?></td>
                            </tr>
                            <tr>
                                <td class="text-center"><?php echo $column_onsale; ?></td>
                                <td class="text-center"><?php echo $column_offsale; ?></td>
                                <td class="text-center"><?php echo $column_total; ?></td>
                                <td class="text-center"><?php echo $column_onsale; ?></td>
                                <td class="text-center"><?php echo $column_offsale; ?></td>
                                <td class="text-center"><?php echo $column_total; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(count($results)){
                                foreach($results as $month => $value){
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $month; ?></td>
                                <td class="text-center"><?php echo $value["albumON"]; ?></td>
                                <td class="text-center"><?php echo $value["albumOFF"]; ?></td>
                                <td class="text-center"><?php echo ($value["albumON"]+$value["albumOFF"]); ?></td>
                                <td class="text-center"><?php echo $value["productON"]; ?></td>
                                <td class="text-center"><?php echo $value["productOFF"]; ?></td>
                                <td class="text-center"><?php echo ($value["productON"]+$value["productOFF"]); ?></td>
                            </tr>
                            <?php }}else{?>
                            <tr>
                                <td class="text-center"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"></script> 
</div>
<?php echo $footer; ?>