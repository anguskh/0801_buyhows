<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" onclick="checkSubmit();"><i class="fa fa-save"></i></a>
                <a href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_preview; ?>" class="btn btn-danger" onclick="previewPage();"><i class="fa fa-eye"></i></a>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-article" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-newsdate"><?php echo $columnNames["newsdate"]; ?></label>
                        <div class="col-sm-4">
                            <div class="input-group date" id="newsdate">
                                <input type="text" name="newsdate" value="<?php echo $newsdate; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-title"><?php echo $columnNames["title"]; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="title" value="<?php echo $title; ?>" id="input-title" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-summary"><?php echo $columnNames["summary"]; ?></label>
                        <div class="col-sm-10">
                            <textarea name="summary" id="input-summary" class="form-control"><?php echo $summary; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-content"><?php echo $columnNames["content"]; ?></label>
                        <div class="col-sm-10">
                            <textarea class="ckeditor" name="content" id="input-content"><?php echo $content; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-summary"><?php echo $columnNames["imgurl"]; ?></label>
                        <div class="col-sm-10">
                            <a href="javascript:(0);" id="thumb-image" class="img-thumbnail">
                                <img src="<?php echo $imgurl; ?>" data-placeholder="<?php echo $placeholder; ?>" alt="" title="" width="100" height="100" />
                            </a>
                            <input type="file" name="imgurl" value="" id="input-image" style="display:none;" onchange="imageChange(this);"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["tags"]; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="tags" value="<?php echo $tags; ?>" data-role="tagsinput"  class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["data_source"]; ?></label>
                        <div class="col-sm-4">
                            <input type="text" name="data_source" value="<?php echo $data_source; ?>" class="form-control" placeholder="例：運動筆記、933樂活網"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["source_url"]; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="source_url" value="<?php echo $source_url; ?>" class="form-control" placeholder="https://i933.com.tw/"/>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["publishstartdate"]; ?></label>
                        <div class="col-sm-4">
                            <div class="input-group date" id="sdate">
                                <input type="text" name="publishstartdate" value="<?php echo $publishstartdate; ?>" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $columnNames["publishenddate"]; ?></label>
                        <div class="col-sm-4">
                            <div class="input-group date" id="edate">
                                <input type="text" name="publishenddate" value="<?php echo $publishenddate; ?>" data-date-format="YYYY-MM-DD HH:mm:ss" placeholder="日期空白表示沒有下架期限" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
  </div>
</div>
<?php echo $footer; ?> 
<link href="view/stylesheet/bootstrap-tagsinput.css" type="text/css" rel="stylesheet" />
<script src="view/javascript/bootstrap-tagsinput.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#newsdate').datetimepicker({
        pickTime: false
    });
    $('#sdate,#edate').datetimepicker();
    $('#form-article').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });
});
function imageChange(obj){
    var ext = $(obj).val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        $(obj).val('');
        alert('上傳圖檔格式錯誤！\n\n 請上傳 gif,png,jpg,jpeg 檔案');
        return false;
    }
    if (obj.files && obj.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#thumb-image img').attr('src', e.target.result);
        }
        reader.readAsDataURL(obj.files[0]);
    }
}
function previewPage(){
    var content = $('[name="content"]').val();
    if(content.length<30){
        alert("內容不足，無法前往預覽！");
    }
    else{
        $('#form-article').attr('action','<?php echo $preview;?>');
        $('#form-article').attr('target','_blank');
        $('#form-article').submit();
    }
}
function checkSubmit(){
    var err = new Array();
    
    if(err.length>0){
        alert(err.join('\n'));
        return false;
    }
    else{
        var url = '<?php echo $action;?>';
        $('#form-article').attr('action',url.replace(/&amp;/gi,"&"));
        $('#form-article').attr('target','_self');
        $('#form-article').submit();
    }
}
</script>