<?php
class ControllerCommonColumnLeft extends Controller {
	public function index() {
        if(isset($this->request->get['act']) && $this->request->get['act']=='ajax'){
            echo $this->change();
            exit;
        }
		if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->load->language('common/column_left');

			$this->load->model('user/user');

			$this->load->model('tool/image');

			$user_info = $this->model_user_user->getUser($this->user->getId());

			if ($user_info) {
				$data['firstname'] = $user_info['firstname'];
				$data['lastname'] = $user_info['lastname'];
				$data['username']  = $user_info['username'];
				$data['user_group'] = $user_info['user_group'];

				if (is_file(DIR_IMAGE . $user_info['image'])) {
					$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				} else {
					$data['image'] = '';
				}
			} else {
				$data['firstname'] = '';
				$data['lastname'] = '';
				$data['user_group'] = '';
				$data['image'] = '';
			}

			$mainMenu = config('auth') ;
			// dump( $mainMenu) ;
			foreach ($mainMenu as $iCnt => $menu) {
				if ( empty($menu['children'])) {
					if ($this->user->hasPermission('access', $menu['folder'].'/'.$menu['controller'])) {
						$data['menus'][] = array(
								'id'       => $menu['id'],
								'icon'	   => $menu['icon'],
								'name'	   => $menu['name'],
								'href'     => $this->url->link($menu['folder'].'/'.$menu['controller'], 'token=' . $this->session->data['token'], true),
								'children' => array()
							);
					}
				} else {
					$childrenArr = $this->buildSubMenu( $menu['children']) ;
					if ( !empty($childrenArr)) {
						$data['menus'][] = array(
								'id'       => $menu['id'],
								'icon'	   => $menu['icon'],
								'name'	   => $menu['name'],
								'href'     => '',
								'children' => $childrenArr,
							) ;
					}
				}
			}

			// 權限管理
			// $user = array();
			// if ($this->user->hasPermission('access', 'user/user')) {
			// 	$user[] = array(
			// 		'name'	   => $this->language->get('text_user'),
			// 		'href'     => $this->url->link('user/user', 'token=' . $this->session->data['token'], true),
			// 		'children' => array()
			// 	);
			// }
			// if ($this->user->hasPermission('access', 'user/user_permission')) {
			// 	$user[] = array(
			// 		'name'	   => $this->language->get('text_user_group'),
			// 		'href'     => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], true),
			// 		'children' => array()
			// 	);
			// }
			// if ($user) {
   //              $data['menus'][] = array(
			// 		'id'       => 'menu-user',
			// 		'icon'	   => 'fa-user',
			// 		'name'	   => $this->language->get('text_users'),
			// 		'href'     => '',
			// 		'children' => $user
			// 	);
			// }

            //左邊選單預設打開
            $data["col_left"] = 'active';
            if(isset($this->session->data['colLeft'])){
                $data["col_left"] = $this->session->data['colLeft'];
            }

            return $this->load->view('common/column_left', $data);
		}
	}

    public function change(){
        if(!isset($this->session->data['colLeft']) || $this->session->data['colLeft']=='active'){
            $this->session->data['colLeft'] = '';
        }
        else{
            $this->session->data['colLeft'] = 'active';
        }

        return $this->session->data['colLeft'];
    }

    /**
     * [buildSubMenu description]
     * @param   array      $data [description]
     * @return  [type]           [description]
     * @Another Angus
     * @date    2020-09-01
     */
    public function buildSubMenu( $data = []) {
    	$retSub = [] ;
    	foreach ($data as $iCnt => $menu) {
    		if ($this->user->hasPermission('access', $menu['folder'].'/'.$menu['controller'])) {
				$retSub[] = array(
					'name'	   => $menu['name'],
					'href'     => $this->url->link($menu['folder'].'/'.$menu['controller'], 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}

    	}

    	return $retSub ;
    }
}
