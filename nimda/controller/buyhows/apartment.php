<?php
class ControllerBuyhowsApartment extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('buyhows/apartment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/apartment');

		$this->getList();
	}

	public function add() {
		$this->load->language('buyhows/apartment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/apartment');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_buyhows_apartment->addBmis($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('buyhows/apartment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/apartment');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_buyhows_apartment->editBmis($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('buyhows/apartment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/apartment');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_buyhows_apartment->deleteBmis($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'username';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('buyhows/apartment/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('buyhows/apartment/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['users'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$user_total = $this->model_buyhows_apartment->getTotalBmis();

		$results = $this->model_buyhows_apartment->getBmisList($filter_data);

		foreach ($results as $result) {
			$data['apartments'][] = array(
				'id'    => $result['id'],
				'mag_committee'   => $result['mag_committee'],
                'approve_date'  => $result['approve_date'],
                'approve_year_c'  => $result['approve_year_c'],
                'approve_year'  => $result['approve_year'],
                'approve_month'  => $result['approve_month'],
                'approve_day'  => $result['approve_day'],
                'reele_date'  => $result['reele_date'],
                'reele_year_c'  => $result['reele_year_c'],
                'reele_year'  => $result['reele_year'],
                'reele_month'  => $result['reele_month'],
                'reele_day'  => $result['reele_day'],
                'chairman'  => $result['chairman'],
                'address'  => $result['address'],
                'city'  => $result['city'],
				'edit'       => $this->url->link('buyhows/apartment/edit', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_mag_committee']    = $this->language->get('column_mag_committee');
        $data['column_approve_date']   = $this->language->get('column_approve_date');
        $data['column_approve_year_c']   = $this->language->get('column_approve_year_c');
        $data['column_approve_year']   = $this->language->get('column_approve_year');
        $data['column_approve_month']   = $this->language->get('column_approve_month');
        $data['column_approve_day']   = $this->language->get('column_approve_day');
		$data['column_reele_date']      = $this->language->get('column_reele_date');
		$data['column_reele_year_c']      = $this->language->get('column_reele_year_c');
		$data['column_reele_year']      = $this->language->get('column_reele_year');
		$data['column_reele_month']      = $this->language->get('column_reele_month');
		$data['column_reele_day']  = $this->language->get('column_reele_day');
		$data['column_chairman']  = $this->language->get('column_chairman');
		$data['column_address']  = $this->language->get('column_address');
		$data['column_city']  = $this->language->get('column_city');
		$data['column_action']  = $this->language->get('column_action');


		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

        
        $data['sort_mag_committee'] = $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . '&sort=mag_committee' . $url, true);
		$data['sort_approve_date']  = $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . '&sort=approve_date' . $url, true);
        $data['sort_reele_date'] = $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . '&sort=reele_date' . $url, true);
		//$data['sort_status'] = $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);
		//$data['sort_date_added'] = $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $user_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($user_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($user_total - $this->config->get('config_limit_admin'))) ? $user_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $user_total, ceil($user_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('buyhows/apartment_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_mag_committee']    = $this->language->get('entry_mag_committee');
        $data['entry_approve_date']   = $this->language->get('entry_approve_date');
        $data['entry_approve_year_c']   = $this->language->get('entry_approve_year_c');
        $data['entry_approve_year']   = $this->language->get('entry_approve_year');
        $data['entry_approve_month']   = $this->language->get('entry_approve_month');
        $data['entry_approve_day']   = $this->language->get('entry_approve_day');
		$data['entry_reele_date']      = $this->language->get('entry_reele_date');
		$data['entry_reele_year_c']      = $this->language->get('entry_reele_year_c');
		$data['entry_reele_year']      = $this->language->get('entry_reele_year');
		$data['entry_reele_month']      = $this->language->get('entry_reele_month');
		$data['entry_reele_day']  = $this->language->get('entry_reele_day');
		$data['entry_chairman']  = $this->language->get('entry_chairman');
		$data['entry_address']  = $this->language->get('entry_address');
		$data['entry_city']  = $this->language->get('entry_city');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('buyhows/apartment/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('buyhows/apartment/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('buyhows/apartment', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$bmis_info = $this->model_buyhows_apartment->getBmis($this->request->get['id']);
		}
		
		$column_arr = array("mag_committee","approve_date","approve_year_c","approve_year","approve_month","approve_day","reele_date","reele_year_c","reele_year","reele_month","reele_day","chairman","address","city");
			
		foreach($column_arr as $arr){
			if (isset($this->request->post[$arr])) {
				$data[$arr] = $this->request->post[$arr];
			} elseif (!empty($bmis_info)) {
				$data[$arr] = $bmis_info[$arr];
			} else {
				$data[$arr] = '';
			}
		}

		$this->load->model('buyhows/apartment');

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}


		$this->load->model('tool/image');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('buyhows/apartment_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'buyhows/apartment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->request->post['mag_committee'] == ""){
			$this->error['mag_committee'] = $this->language->get('error_mag_committee');

		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'buyhows/apartment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['selected'] as $id) {
			if ($this->user->getId() == $id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}

		return !$this->error;
	}
}