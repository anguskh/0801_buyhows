<?php
class ControllerBuyhowsNetprice extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('buyhows/netprice');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/netprice');

		$this->getList();
	}

	public function add() {
		$this->load->language('buyhows/netprice');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/netprice');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_buyhows_netprice->addMap_main($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('buyhows/netprice');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/netprice');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_buyhows_netprice->editMap_main($this->request->get['sn'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('buyhows/netprice');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('buyhows/netprice');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_buyhows_netprice->deleteMap_main($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'username';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('buyhows/netprice/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('buyhows/netprice/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['users'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$user_total = $this->model_buyhows_netprice->getTotalMap_main();

		$results = $this->model_buyhows_netprice->getMap_mainList($filter_data);

		foreach ($results as $result) {
			$data['netprices'][] = array(
				'sn'    => $result['sn'],
				'tw_city'   => $result['tw_city'],
				'buy_type'   => $result['buy_type'],
                'tw_city_area'  => $result['tw_city_area'],
                'address'  => $result['address'],
                'land_turn_area'  => $result['land_turn_area'],
                'land_use_type'  => $result['land_use_type'],
                'land_use_type_none'  => $result['land_use_type_none'],
                'land_use_type_none_code'  => $result['land_use_type_none_code'],
                'jiaoyi_date'  => $result['jiaoyi_date'],
                'transfer_target'  => $result['transfer_target'],
                'total_level'  => $result['total_level'],
                'building_type'  => $result['building_type'],
                'main_use'  => $result['main_use'],
                'materials'  => $result['materials'],
                'completed_date'  => $result['completed_date'],
                'jiaoyi_date_t'  => $result['jiaoyi_date_t'],
                'transfer_total_area'  => $result['transfer_total_area'],
                'structure_building_bedroom'  => $result['structure_building_bedroom'],
                'structure_building_apartment'  => $result['structure_building_apartment'],
                'structure_building_bathroom'  => $result['structure_building_bathroom'],
                'structure_building_part'  => $result['structure_building_part'],
                'committee'  => $result['committee'],
                'total_price'  => $result['total_price'],
                'price_square'  => $result['price_square'],
                'parking_type'  => $result['parking_type'],
                'parking_square'  => $result['parking_square'],
                'parking_price'  => $result['parking_price'],
                'memo'  => $result['memo'],
				'edit'       => $this->url->link('buyhows/netprice/edit', 'token=' . $this->session->data['token'] . '&sn=' . $result['sn'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_tw_city']  = $this->language->get('column_tw_city');
		$data['column_tw_city_area']   = $this->language->get('column_tw_city_area');
		$data['column_buy_type'] = $this->language->get('column_buy_type');
		$data['column_address']  = $this->language->get('column_address');
		$data['column_land_turn_area'] = $this->language->get('column_land_turn_area');
		$data['column_land_use_type']= $this->language->get('column_land_use_type');
		$data['column_land_use_type_none']= $this->language->get('column_land_use_type_none');
		$data['column_land_use_type_none_code'] = $this->language->get('column_land_use_type_none_code');
		$data['column_jiaoyi_date']= $this->language->get('column_jiaoyi_date');
		$data['column_transfer_target'] = $this->language->get('column_transfer_target');
		$data['column_land_use_type_none_code'] = $this->language->get('column_land_use_type_none_code');
		$data['column_total_level'] = $this->language->get('column_total_level');
		$data['column_building_type']= $this->language->get('column_building_type');
		$data['column_main_use'] = $this->language->get('column_main_use');
		$data['column_materials']= $this->language->get('column_materials');
		$data['column_completed_date']= $this->language->get('column_completed_date');
		$data['column_jiaoyi_date_t']= $this->language->get('column_jiaoyi_date_t');
		$data['column_transfer_total_area']= $this->language->get('column_transfer_total_area');
		$data['column_structure_building_bedroom']= $this->language->get('column_structure_building_bedroom');
		$data['column_structure_building_apartment'] = $this->language->get('column_structure_building_apartment');
		$data['column_structure_building_bathroom']= $this->language->get('column_structure_building_bathroom');
		$data['column_structure_building_part'] = $this->language->get('column_structure_building_part');
		$data['column_committee']= $this->language->get('column_committee');
		$data['column_total_price'] = $this->language->get('column_total_price');
		$data['column_price_square']= $this->language->get('column_price_square');
		$data['column_parking_type'] = $this->language->get('column_parking_type');
		$data['column_parking_square'] = $this->language->get('column_parking_square');
		$data['column_parking_price']= $this->language->get('column_parking_price');
		$data['column_memo'] = $this->language->get('column_memo');
		$data['column_action']  = $this->language->get('column_action');


		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

        
        $data['sort_mag_committee'] = $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . '&sort=mag_committee' . $url, true);
		$data['sort_approve_date']  = $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . '&sort=approve_date' . $url, true);
        $data['sort_reele_date'] = $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . '&sort=reele_date' . $url, true);
		//$data['sort_status'] = $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);
		//$data['sort_date_added'] = $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $user_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($user_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($user_total - $this->config->get('config_limit_admin'))) ? $user_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $user_total, ceil($user_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('buyhows/netprice_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['sn']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_tw_city']  = $this->language->get('entry_tw_city');
		$data['entry_tw_city_area']   = $this->language->get('entry_tw_city_area');
		$data['entry_buy_type']  = $this->language->get('entry_buy_type');
		$data['entry_address']  = $this->language->get('entry_address');
		$data['entry_land_turn_area'] = $this->language->get('entry_land_turn_area');
		$data['entry_land_use_type'] = $this->language->get('entry_land_use_type');
		$data['entry_land_use_type_none'] = $this->language->get('entry_land_use_type_none');
		$data['entry_land_use_type_none_code'] = $this->language->get('entry_land_use_type_none_code');
		$data['entry_jiaoyi_date'] = $this->language->get('entry_jiaoyi_date');
		$data['entry_transfer_target'] = $this->language->get('entry_transfer_target');
		$data['entry_land_use_type_none_code'] = $this->language->get('entry_land_use_type_none_code');
		$data['entry_total_level'] = $this->language->get('entry_total_level');
		$data['entry_building_type'] = $this->language->get('entry_building_type');
		$data['entry_main_use'] = $this->language->get('entry_main_use');
		$data['entry_materials'] = $this->language->get('entry_materials');
		$data['entry_completed_date'] = $this->language->get('entry_completed_date');
		$data['entry_jiaoyi_date_t'] = $this->language->get('entry_jiaoyi_date_t');
		$data['entry_transfer_total_area'] = $this->language->get('entry_transfer_total_area');
		$data['entry_structure_building_bedroom'] = $this->language->get('entry_structure_building_bedroom');
		$data['entry_structure_building_apartment'] = $this->language->get('entry_structure_building_apartment');
		$data['entry_structure_building_bathroom'] = $this->language->get('entry_structure_building_bathroom');
		$data['entry_structure_building_part'] = $this->language->get('entry_structure_building_part');
		$data['entry_committee'] = $this->language->get('entry_committee');
		$data['entry_total_price'] = $this->language->get('entry_total_price');
		$data['entry_price_square'] = $this->language->get('entry_price_square');
		$data['entry_parking_type'] = $this->language->get('entry_parking_type');
		$data['entry_parking_square'] = $this->language->get('entry_parking_square');
		$data['entry_parking_price'] = $this->language->get('entry_parking_price');
		$data['entry_memo'] = $this->language->get('entry_memo');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['sn'])) {
			$data['action'] = $this->url->link('buyhows/netprice/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('buyhows/netprice/edit', 'token=' . $this->session->data['token'] . '&sn=' . $this->request->get['sn'] . $url, true);
		}

		$data['cancel'] = $this->url->link('buyhows/netprice', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['sn']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$bmis_info = $this->model_buyhows_netprice->getMap_main($this->request->get['sn']);
		}
		
		$column_arr = array("tw_city","tw_city_area","buy_type","address","land_turn_area","land_use_type","land_use_type_none","land_use_type_none_code","jiaoyi_date","transfer_target","land_use_type_none_code","total_level","building_type","main_use","materials","completed_date","jiaoyi_date_t","transfer_total_area","structure_building_bedroom","structure_building_apartment","structure_building_bathroom","structure_building_part","committee","total_price","price_square","parking_type","parking_square","parking_price","memo");


		foreach($column_arr as $arr){
			if (isset($this->request->post[$arr])) {
				$data[$arr] = $this->request->post[$arr];
			} elseif (!empty($bmis_info)) {
				$data[$arr] = $bmis_info[$arr];
			} else {
				$data[$arr] = '';
			}
		}

		$this->load->model('buyhows/netprice');

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}


		$this->load->model('tool/image');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('buyhows/netprice_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'buyhows/netprice')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}


		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'buyhows/netprice')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['selected'] as $id) {
			if ($this->user->getId() == $id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}

		return !$this->error;
	}
}