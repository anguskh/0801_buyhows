<?php
class ControllerBuyhowsTwcity extends Controller {
	private $error = array();
	private $retTwDivision = [] ;

	private $heading_title = "縣市管理" ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-09-24
	 */
	public function index() {
		// $this->load->language('buyhows/apartment');

		$this->document->setTitle( $this->heading_title) ;

		// $this->load->model('buyhows/apartment');

		$this->getList();
	}

	/**
	 * [save description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-09-25
	 */
	public function save() {
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			mac_arr2file('tw_division.php', $this->retTwDivision) ;
			$this->session->data['success'] = '設定已完成';
		}

		$this->document->setTitle( $this->heading_title) ;
		$this->getList();
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-09-24
	 */
	private function getList() {
		$url = '';

		$data['heading_title'] = $this->heading_title ;
		$data['text_list']     = '列表' ;
		$data['breadcrumbs']   = array();
		$data['breadcrumbs'][] = array(
			'text' => '首頁',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->heading_title,
			'href' => $this->url->link('buyhows/twcity', 'token=' . $this->session->data['token'] . $url, true)
		);
		$data['action'] = $this->url->link('buyhows/twcity/save', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['tw_division'] = config('tw_division') ;
		// dump( $data['tw_division']) ;


		$data['pagination'] = '' ;
		$data['results']    = '' ;

		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('buyhows/tw_division_list', $data));
	}

	/**
	 * [validateForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-09-25
	 */
	private function validateForm() {
		$this->retTwDivision = $this->request->post['tw_anme'] ;
		return true ;
	}
}