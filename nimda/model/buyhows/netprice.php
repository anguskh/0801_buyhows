<?php
class ModelBuyhowsnetprice extends Model {
	public function addMap_main($data) {
		$this->db->query("INSERT INTO `map_main` SET tw_city = '" . $this->db->escape($data['tw_city']) . "', tw_city_area = '" . $this->db->escape($data['tw_city_area']) . "', buy_type = '" . $this->db->escape($data['buy_type']) . "', address = '" . $this->db->escape($data['address']) . "', land_turn_area = '" . $this->db->escape($data['land_turn_area']) . "', land_use_type = '" . $this->db->escape($data['land_use_type']) . "', land_use_type_none = '" . $this->db->escape($data['land_use_type_none']) . "', land_use_type_none_code = '" . $this->db->escape($data['land_use_type_none_code']) . "', total_level = '" . $this->db->escape($data['total_level']) . "', building_type = '" . $this->db->escape($data['building_type']) . "',  main_use = '" . $this->db->escape($data['main_use']) . "', materials = '" . $this->db->escape($data['materials']) . "', completed_date = '" . $this->db->escape($data['completed_date']) . "', transfer_target = '" . $this->db->escape($data['transfer_target']) . "', jiaoyi_date = '" . $this->db->escape($data['jiaoyi_date']) . "', transfer_total_area = '" . $this->db->escape($data['transfer_total_area']) . "',structure_building_bedroom = '" . $this->db->escape($data['structure_building_bedroom']) . "', structure_building_apartment = '" . $this->db->escape($data['structure_building_apartment']) . "', structure_building_bathroom = '" . $this->db->escape($data['structure_building_bathroom']) . "', structure_building_part = '" . $this->db->escape($data['structure_building_part']) . "', committee = '" . $this->db->escape($data['committee']) . "', total_price = '" . $this->db->escape($data['total_price']) . "', price_square = '" . $this->db->escape($data['price_square']) . "', parking_type = '" . $this->db->escape($data['parking_type']) . "', parking_square = '" . $this->db->escape($data['parking_square']) . "', parking_price = '" . $this->db->escape($data['parking_price']) . "', memo = '" . $this->db->escape($data['memo']) . "'");

		return $this->db->getLastId();
	}

	public function editMap_main($map_sn, $data) {
		$this->db->query("UPDATE `map_main` SET tw_city = '" . $this->db->escape($data['tw_city']) . "', tw_city_area = '" . $this->db->escape($data['tw_city_area']) . "', buy_type = '" . $this->db->escape($data['buy_type']) . "', address = '" . $this->db->escape($data['address']) . "', land_turn_area = '" . $this->db->escape($data['land_turn_area']) . "', land_use_type = '" . $this->db->escape($data['land_use_type']) . "', land_use_type_none = '" . $this->db->escape($data['land_use_type_none']) . "', land_use_type_none_code = '" . $this->db->escape($data['land_use_type_none_code']) . "', total_level = '" . $this->db->escape($data['total_level']) . "', building_type = '" . $this->db->escape($data['building_type']) . "',  main_use = '" . $this->db->escape($data['main_use']) . "', materials = '" . $this->db->escape($data['materials']) . "', completed_date = '" . $this->db->escape($data['completed_date']) . "', transfer_target = '" . $this->db->escape($data['transfer_target']) . "', jiaoyi_date = '" . $this->db->escape($data['jiaoyi_date']) . "', jiaoyi_date_t = '" . $this->db->escape($data['jiaoyi_date_t']) . "', transfer_total_area = '" . $this->db->escape($data['transfer_total_area']) . "',structure_building_bedroom = '" . $this->db->escape($data['structure_building_bedroom']) . "', structure_building_apartment = '" . $this->db->escape($data['structure_building_apartment']) . "', structure_building_bathroom = '" . $this->db->escape($data['structure_building_bathroom']) . "', structure_building_part = '" . $this->db->escape($data['structure_building_part']) . "', committee = '" . $this->db->escape($data['committee']) . "', total_price = '" . $this->db->escape($data['total_price']) . "', price_square = '" . $this->db->escape($data['price_square']) . "', parking_type = '" . $this->db->escape($data['parking_type']) . "', parking_square = '" . $this->db->escape($data['parking_square']) . "', parking_price = '" . $this->db->escape($data['parking_price']) . "', memo = '" . $this->db->escape($data['memo']) . "' WHERE sn = '" . $map_sn . "'");
	}


	public function deleteMap_main($map_sn) {
		$this->db->query("DELETE FROM `map_main` WHERE sn = '" . $map_sn . "'");
	}

	public function getMap_main($map_sn) {
		$query = $this->db->query("SELECT * FROM `map_main` u WHERE u.sn = '" . $map_sn . "'");

		return $query->row;
	}


	public function getMap_mainList($data = array()) {
		$sql = "SELECT * FROM `map_main`";

		$sort_data = array(
			'mag_committee',
			'approve_date',
			'reele_date'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sn";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalMap_main() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `map_main`");

		return $query->row['total'];
	}

}