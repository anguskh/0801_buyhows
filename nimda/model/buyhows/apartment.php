<?php
class ModelBuyhowsapartment extends Model {
	public function addBmis($data) {
		$this->db->query("INSERT INTO `tb_bmis` SET mag_committee = '" . $this->db->escape($data['mag_committee']) . "', approve_date = '" . $this->db->escape($data['approve_date']) . "', approve_year_c = '" . $this->db->escape($data['approve_year_c']) . "', approve_year = '" . $this->db->escape($data['approve_year']) . "', approve_month = '" . $this->db->escape($data['approve_month']) . "', approve_day = '" . $this->db->escape($data['approve_day']) . "', reele_date = '" . $this->db->escape($data['reele_date']) . "', reele_year_c = '" . $this->db->escape($data['reele_year_c']) . "', reele_year = '" . $this->db->escape($data['reele_year']) . "', reele_month = '" . $this->db->escape($data['reele_month']) . "',  reele_day = '" . $this->db->escape($data['reele_day']) . "', chairman = '" . $this->db->escape($data['chairman']) . "', address = '" . $this->db->escape($data['address']) . "', city = '" . $this->db->escape($data['city']) . "'");
	
		return $this->db->getLastId();
	}

	public function editBmis($bmis_id, $data) {
		$this->db->query("UPDATE `tb_bmis` SET mag_committee = '" . $this->db->escape($data['mag_committee']) . "', approve_date = '" . $this->db->escape($data['approve_date']) . "', approve_year_c = '" . $this->db->escape($data['approve_year_c']) . "', approve_year = '" . $this->db->escape($data['approve_year']) . "', approve_month = '" . $this->db->escape($data['approve_month']) . "', approve_day = '" . $this->db->escape($data['approve_day']) . "', reele_date = '" . $this->db->escape($data['reele_date']) . "', reele_year_c = '" . $this->db->escape($data['reele_year_c']) . "', reele_year = '" . $this->db->escape($data['reele_year']) . "', reele_month = '" . $this->db->escape($data['reele_month']) . "',  reele_day = '" . $this->db->escape($data['reele_day']) . "', chairman = '" . $this->db->escape($data['chairman']) . "', address = '" . $this->db->escape($data['address']) . "', city = '" . $this->db->escape($data['city']) . "' WHERE id = '" . $bmis_id . "'");
	}


	public function deleteBmis($bmis_id) {
		$this->db->query("DELETE FROM `tb_bmis` WHERE id = '" . $bmis_id . "'");
	}

	public function getBmis($bmis_id) {
		$query = $this->db->query("SELECT * FROM `tb_bmis` u WHERE u.id = '" . $bmis_id . "'");

		return $query->row;
	}


	public function getBmisList($data = array()) {
		$sql = "SELECT * FROM `tb_bmis`";

		$sort_data = array(
			'mag_committee',
			'approve_date',
			'reele_date'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalBmis() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `tb_bmis`");

		return $query->row['total'];
	}

}