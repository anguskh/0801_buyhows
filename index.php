<?php
//header("Location: nimda/index.php") ;
//exit() ;
$serverUrl = $_SERVER['REQUEST_URI'] ;
$pattern = '/(\?route=.*)/';
preg_match( $pattern, $serverUrl, $matches);
// var_dump( $matches) ;
if ( !isset($matches[1])) {
	header("Location: index.html");
    exit();
}


// Version
define('VERSION', '2.3.0.3_rc');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('catalog');